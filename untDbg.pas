unit untDbg;

interface
     uses Winapi.Windows, System.SysUtils,System.Classes, Vcl.Controls, Vcl.Dialogs,System.SyncObjs,TlHelp32, PsAPI,
     PiFastList,DbgDef,jwaImageHlp,jwaNtStatus,jwawinnt;

type
   PDebugWin = ^TDebugWin;

   TDebugWin = class(TThread)
   private
     FStartupInfo            : STARTUPINFO;
     FProcessInfo            : PROCESS_INFORMATION;
     Fm_dbgPI                : PROCESS_INFORMATION;
     FDebugEvent             : _DEBUG_EVENT;

     FBPXs                   : TFastList;
     FBPMs                   : TFastList;
     FBPHs                   : TFastList;
     FTIDs                   : TFastList;
     FPIDs                   : TFastList;
     FDLLs                   : TFastList;
     FExceptHandler          : TFastList;
     FpCtx                   : WinApi.Windows.PContext;
     FWowProcessContext      : WinApi.Windows.PContext;

     FszFileName             : string;
     FszCommandLine          : string;
     FIsDebug                : Boolean;
     FNormalDebugging        : Boolean;
     FbStopDebugging         : Boolean;
     FbSingleStepFlag        : Boolean;
     Fm_debuggerBreak        : Boolean;
     FhDbgEvent              : THandle;
     FhCurProc               : THandle;
  	 Fm_waitForGUI           : THandle;
     FdwPidToAttach          : DWORD;
     FdwCurPID               : THandle;
     FdwCurTID               : THandle;
     Fm_continueWithException: Integer;
     FDbgSetting             : TDbgSettings;


     CallBackCustomBreakPoint: TCallBackCustomBreakPoint;
     CustomExceptEvent       : TCallBackCustomExceptEvent;
     FStepCallBack           : Pointer;
     FAttacCallBack          : Pointer;
     FEPCallBack             : Pointer;

     FOnDebuggerEnd          : TOnDebuggerEnd;
     FOnDebuggerBreak        : TOnDebuggerBreak;
     FOnNewBPAdded           : TOnNewBPAdded;
     FOnBPDeleted            : TOnBPDeleted;
     FOnDll                  : TOnDll;
     FOnPID                  : TOnPID;
     FOnNewPID               : TOnNewPID;
     FOnThread               : TOnThread;
     FOnDbgString            : TOnDbgString;
     FOnLog                  : TOnDbgLogEvent;
     FMsg                    : string;
     FTime                   : TTime;
     // api windows
     OpenThread              : TOpenThread ;

     procedure DebugLoop;
     procedure AttachedDebugging;
     procedure NormalDebugging;
     procedure CleanWorkSpace;
     procedure UpdateOffsetsBPs;
     function  CalcOffsetForModule(var moduleName: PChar; Offset: NativeUInt; PID: DWORD): NativeUInt;
     // procedure UpdateOffsetsBPs;
     function  PBThreadInfo(dwPID, dwTID: DWORD; dwEP: NativeUInt; bSuspended: Boolean; dwExitCode: DWORD; bNew: Boolean): Boolean;
     function  PBProcInfo(dwPID: DWORD; sFileName:string; dwEP,dwIBase: NativeUInt;dwExitCode: DWORD; hProc: THandle):Boolean;
     function  PBExceptionInfo(dwExceptionOffset, dwExceptionCode: NativeUInt; dwPID, dwTID: DWORD): Boolean;
     function  PBDLLInfo(sDLLPath: string; dwPID: DWORD; dwEP: NativeUInt; bLoaded: Boolean): Boolean;
     procedure PBLogInfo;
     function  PBDbgString(sMessage: string; dwPID: DWORD): Boolean;
     function  SetBPX(dwPID: DWORD; bpxAddress: NativeUInt;dwKeep,dwSize: DWORD; var bOrgByte : byte): Boolean;
     function  dBPX(const dwPID: DWORD; dwOffset: NativeUint; dwSize: DWORD; btOrgByte: Byte): Boolean;
     function  SetMemoryBP(dwPID: DWORD; MemoryStart : NativeUInt;SizeOfMemory,dwKeep: DWORD ):Boolean;
     function  dMemoryBP(dwPId: DWORD; MemoryStart, SizeOfMemory: Integer): Boolean;
     function  SetHardwareBreakPoint(dwPID : DWORD; bpxAddress: NativeUInt;bpxSize,bpxSlot, bpxFlag : DWORD):Boolean;
     function  dHardwareBP(dwPID: DWORD; bpxAddress: NativeUInt; bpxSlot: DWORD): Boolean;
     function  InitBP: Boolean;
     function  CheckProcessState(dwPID: DWORD): Boolean;
     function  CheckIfExceptionIsBP(dwExceptionOffset,dwExceptionType:NativeUInt; dwPID:DWORD; bClearTrapFlag:Boolean): Boolean;
     function  SuspendProcess(dwPID: DWORD; bSuspend: Boolean): Boolean;
     function  SetThreadContextHelper(bDecIP, bSetTrapFlag: Boolean; dwThreadID, dwPID: DWORD): Boolean;
     function  IsDebuggerSuspended: Boolean;
     function  GetCurrentProcessHandle(dwPID: DWORD): THandle;overload;
     function  CallBreakDebugger(debugEvent: TDEBUGEVENT; dwHandle: DWORD;CallBack: Pointer = nil): DWORD ;
     function  GetMainThreadID: DWORD;
     function  GetMainProcessID: DWORD;
     function  GetFileNameFromHandle(Handle: THandle): String;
     // Handle Eventi debug
     function  HandleExceptionDebugEvent(debugEvent: TDEBUGEVENT): Cardinal;
     function  CreateThreadDebugEvent(   debugEvent: TDEBUGEVENT): Cardinal;
     function  CreateProcessDebugEvent(  debugEvent: TDEBUGEVENT): Cardinal;
     function  ExitThreadDebugEvent(     debugevent: TDEBUGEVENT): Cardinal;
     function  ExitProcessDebugEvent(    debugEvent: TDEBUGEVENT; var bContinueDebugging : Boolean): Cardinal;
     function  LoadDLLDebugEvent(        debugEvent: TDEBUGEVENT): Cardinal;
     function  UnloadDLLDebugEvent(      debugEvent: TDEBUGEVENT): Cardinal;
     function  OutputDebugStringEvent(   debugEvent: TDEBUGEVENT): Cardinal;

     function  GetContextDataEx(hActiveThread: THandle; IndexOfRegister: DWORD): LongInt;
     function  SetContextDataEx(hActiveThread: THandle; IndexOfRegister: DWORD; NewRegValue: Integer): Boolean;
     function  EnableDebugFlag: Boolean;
     procedure DoDbgBreak;
     procedure DoOnDbgEnd;
     procedure SetDefaultSetting;
     function  GetProcessHandleByPID(dwPID: DWORD): THandle;
     procedure HandleForException(handleException: Integer);
     procedure SetNewThreadContext(isWow64: Boolean; newProcessContext,newWowProcessContext: Winapi.Windows.PContext);

  protected
     procedure Execute; override;
  public
     constructor Create;  overload;
     constructor Create(sTarget:string); overload;
     destructor  Destroy;  override;
     // Funzione di get/set
     procedure   SetWaitForUserEvent;
     function    ReadMemoryFromDebugee(dwPID: DWORD; dwAddress: nativeUInt; dwSize: DWORD; lpBuffer: Pointer): Boolean;
     function    WriteMemoryFromDebugee(dwPID: DWORD; dwAddress: NativeUInt; dwSize: DWORD; lpBuffer: Pointer): Boolean;
     function    GetContextData(IndexOfRegister: DWORD): LongInt;
     function    SetContextData(IndexOfRegister: DWORD; NewRegValue: LongInt): Boolean;
     function    GetDebugData: Pointer;
     function    GetProcessInformation: Pointer;
     function    GetDebuggingState: Boolean;
     function    GetCurrentPID: DWORD;
     function    GetCurrentTID: DWORD;
     function    GetCurrentProcessHandle: THandle;  overload;
     function    GetTarget: string;
     procedure   SetTarget(sTarget: string);
     function    IsTargetSet: Boolean;
     function    GetCommandLine: string;
     procedure   ClearCommandLine;
     procedure   ClearTarget;
     procedure   SetCommandLine(CommandLine: string);
     function    IsOffsetAnBP(dwOffset: NativeUInt): Boolean;
     function    IsOffsetAnEP(dwOffset: NativeUInt): Boolean;
     // Gestione debug
     function    StopDebuggingAll: Boolean;
     function    StopDebugging(dwPID: DWORD): Boolean;
     function    StartDebugging: Boolean;
     function    AttachToProcess(dwPID: DWORD): Boolean;
     function    DetachFromProcess:Boolean;
     function    SuspendDebuggingAll: Boolean;
     function    ResumeDebugging: Boolean;
     function    RestartDebugging: Boolean;
     function    SuspendDebugging(dwPID: DWORD): Boolean;
     function    StepOver(dwNewOffset: DWORD; StepCallBack: Pointer):Boolean;
     Function    StepInto(StepCallBack: Pointer;bRemove: Boolean = False):Boolean;
     // funzioni Gestione breakpoint
     procedure   AddBreakpointToList(dwBPType, dwTypeFlag, dwPID: DWORD; dwOffset: NativeUInt; dwSlot, dwKeep: DWORD;pBPCallBack: Pointer);
     function    RemoveBPFromList(dwOffset: NativeUInt; dwType: DWORD): Boolean;
     function    RemoveBPs: Boolean;
     // funzioni gestione Eccezzioni
     procedure   CustomExceptionAdd(dwExceptionType, dwAction: DWORD; pHandler: Pointer);
     procedure   CustomExceptionRemove(dwExceptionType: DWORD);
     procedure   CustomExceptionRemoveAll;

     // Eventi
     property OnDbgLog        : TOnDbgLogEvent   read FOnLog            write FOnLog;
     property OnDbgString     : TOnDbgString     read FOnDbgString      write FOnDbgString;
     property OnThread        : TOnThread        read FOnThread         write FOnThread;
     property OnPID           : TOnPID           read FOnPID            write FOnPID;
     property OnNewPID        : TOnNewPID        read FOnNewPID         write FOnNewPID;
     property OnDll           : TOnDll           read FOnDll            write FOnDll;
     property OnDbgBreak      : TOnDebuggerBreak read FOnDebuggerBreak  write FOnDebuggerBreak;
     property OnDbgEnd        : TOnDebuggerEnd   read FOnDebuggerEnd    write FOnDebuggerEnd;
     property OnNewBPAdded    : TOnNewBPAdded    read FOnNewBPAdded     write FOnNewBPAdded;
     property OnBPDeleted     : TOnBPDeleted     read FOnBPDeleted      write FOnBPDeleted;

     property BPXList         : TFastList        read FBPXs;
     property DbgSetting      : TDbgSettings     read FDbgSetting       write FDbgSetting;
     property bStopDebugging  : Boolean          read FbStopDebugging   write FbStopDebugging;
     property EPCallBack      : Pointer          read FEPCallBack       write FEPCallBack;
     property ProcessContext  : WinApi.Windows.PContext              read FpCtx;
end;

implementation


resourcestring
      rsDebugEnd            = '"[-] Debugging finished!"';
      rsExitProcess         = '[-] Exit Process(%X) with Exitcode: $%0.8X';
      rsNewProcess          = '[+] New Process(%X) with Entrypoint: $%0.8X';
      rsExitThread          = '[-] Exit Thread(%X) in Process(%X) with Exitcode: %08X';
      rsNewThread           = '[+] New Thread(%X) in Process(%X) with Entrypoint: $%0.8X';
      rsExceptionInfo       = '[!] ExceptionCode: %08X ExceptionOffset: %08X PID: %X TID: %X';
      rsUnLoadDLL           = '[+] PID(%X) - Unloaded DLL: %s';
      rsLoadDLL             = '[+] PID(%X) - Loaded DLL: %s Entrypoint: %08X';
      rsDebugSuspend        = '[!] %X Debugging suspended!';
      rsAttachProc          = '[+] Attached to Process';
      rsNotLoadSymbol       = '[!] Could not load symbols for Process(%0.8X)';
      rsNotLoadSymbolDLL    = '[!] Could not load symbols for DLL: %s';
      rsBreakOnEP           = '[!] Break on Entry Point - PID %06X - $%0.8X';
      rsBreakOnSoftwareBP   = '[!] Break on Software BP - PID %06X - $%0.8X';
      rsRestoreBP           = '[!] Restored BP - PID: %0.6X - $%0.8X';
      rsBreakOnHBP          = '[!] Break on Hardware BP - PID: %0.6X - $%0.8X';
      rsBreakOnMemoryBP     = '[!] Break on Memory BP - PID: %0.6X - $%0.8X';
      rsInsertNewHBP        = '[+] New HardwareBP in %d Threads placed at %X in Slot No. %d';

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.SetWaitForUserEvent
  Author:    Max
  DateTime:  2013.09.08
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TDebugWin.SetWaitForUserEvent;
(*******************************************************************************)
begin
     FDbgSetting.bNotWaitEventDbg := False;
end;

{-------------------------------------------------------------------------------
  Procedure: CalcOffsetForModule
  Author:    Max
  DateTime:  2013.08.22
  Arguments: moduleName: PChar; Offset: NativeUInt; PID: DWORD
  Result:    NativeUInt
-------------------------------------------------------------------------------}
function TDebugWin.CalcOffsetForModule(var moduleName: PChar; Offset: NativeUInt; PID: DWORD): NativeUInt;
(*******************************************************************************)
var
  hProc             : THandle;
	sTemp2     : Array[0..MAX_PATH] of Char;
  dwAddress, dwBase : NativeUInt;
  memInfo           : winapi.Windows.MEMORY_BASIC_INFORMATION;
  modName, sTemp : String;
begin
     dwAddress := 0;
     dwBase    := 0;
     sTemp     := '';

     hProc :=  GetProcessHandleByPID(PID) ;
     while VirtualQueryEx(hProc,Pointer(dwAddress),memInfo,sizeof(memInfo)) <> 0 do
     begin
          GetMappedFileName(hProc, pointer(dwAddress), sTemp2, SizeOf(sTemp2) div SizeOf(Char));
          modName := LowerCase(StrPas(sTemp2));
          modName := ExtractFileName(modName);

          if modName <> '' then
          begin
               if dwBase = 0 then dwBase := NativeUInt(memInfo.BaseAddress);
               if (sTemp = '') and  (Offset > NativeUInt(memInfo.BaseAddress)) and (Offset < NativeUInt(memInfo.BaseAddress) + memInfo.RegionSize) then
               begin
                    sTemp      := modName;
                    moduleName := PChar(sTemp) ;
                    Result     := dwBase;
                    Exit;
               end
               else if sTemp = modName then
               begin
                    Result     := dwBase;
                    Exit;
               end;
          end else
          begin
               dwBase := 0;
          end;
          dwAddress := dwAddress + memInfo.RegionSize;
     end;
     Result := Offset;

end;
{TDegugWin}

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.Create
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
constructor TDebugWin.Create;
(*******************************************************************************)
begin
    inherited Create(True);

    ZeroMemory(@FProcessInfo,sizeof(FProcessInfo));
    ZeroMemory(@FStartupInfo,sizeof(STARTUPINFO));
    FStartupInfo.cb := sizeof(FStartupInfo);

    GetMem(FpCtx,sizeof(TCONTEXT));

    // Eventi per notificare azioni
    FOnLog          := nil;
    FOnDll          := nil;
    FOnPID          := nil;
    FOnThread       := nil;
    FOnDbgString    := nil;
    // CallBack
    FStepCallBack   := nil;
    FAttacCallBack  := nil;
    FEPCallBack     := nil;

    FBPXs          := TFastList.Create;
    FBPMs          := TFastList.Create;
    FBPHs          := TFastList.Create;
    FTIDs          := TFastList.Create;
    FPIDs          := TFastList.Create;
    FDLLs          := TFastList.Create;
    FExceptHandler := TFastList.Create;
    FIsDebug       := False;
    FszFileName    := '';
    FszCommandLine := '';
    FMsg           := '';

    OpenThread := GetProcAddress(GetModuleHandleA('kernel32.dll'),'OpenThread');

    FbSingleStepFlag := False;
    FbStopDebugging  := False;
    FNormalDebugging := True;
	  FIsDebug         := False;
    Fm_debuggerBreak := False;

	  SetDefaultSetting;
    EnableDebugFlag;

    Fm_waitForGUI := CreateEvent(nil,false,false,'hWaitForGUI');
   	FhDbgEvent    := CreateEvent(nil,false,false,'hDebugEvent');

end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.Create
  Author:    Max
  DateTime:  2013.08.27
  Arguments: sTarget:string
  Result:    None
-------------------------------------------------------------------------------}
constructor TDebugWin.Create(sTarget:string);
(*******************************************************************************)
begin
    inherited Create(True);

    ZeroMemory(@FProcessInfo,sizeof(FProcessInfo));
    ZeroMemory(@FStartupInfo,sizeof(STARTUPINFO));
    FStartupInfo.cb := sizeof(FStartupInfo);

    GetMem(FpCtx,sizeof(TCONTEXT));

    // Eventi per notificare azioni
    FOnLog         := nil;
    // CallBAck
    FStepCallBack  := nil;
    FAttacCallBack := nil;
    FEPCallBack    := nil;


    FBPXs          := TFastList.Create;
    FBPMs          := TFastList.Create;
    FBPHs          := TFastList.Create;
    FTIDs          := TFastList.Create;
    FPIDs          := TFastList.Create;
    FDLLs          := TFastList.Create;
    FExceptHandler := TFastList.Create;
    FIsDebug       := False;
    FszFileName    := sTarget;
    FszCommandLine := '';
    FMsg           := '';

    OpenThread := GetProcAddress(GetModuleHandleA('kernel32.dll'),'OpenThread');

    FbSingleStepFlag := False;
    FbStopDebugging  := False;
    FNormalDebugging := True;
	  FIsDebug         := False;
    Fm_debuggerBreak := False;

    SetDefaultSetting;
	  EnableDebugFlag;

    Fm_waitForGUI := CreateEvent(nil,false,false,'hWaitForGUI');
   	FhDbgEvent    := CreateEvent(nil,false,false,'hDebugEvent');

end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.Destroy
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
destructor TDebugWin.Destroy;
(*******************************************************************************)
begin

     FreeMem(FpCtx);

     CallBackCustomBreakPoint:= nil ;
     CustomExceptEvent       := nil;
     FStepCallBack           := nil;
     FAttacCallBack          := nil;
     FEPCallBack             := nil;
     FOnLog                  := nil;

     RemoveBPs;
     CleanWorkSpace;
     CustomExceptionRemoveAll;

     FreeAndNil(FBPXs);
     FreeAndNil(FBPMs);
     FreeAndNil(FBPHs);
     FreeAndNil(FTIDs);
     FreeAndNil(FPIDs);
     FreeAndNil(FDLLs);
     FreeAndNil(FExceptHandler);

     CloseHandle(Fm_waitForGUI);
	   CloseHandle(FhDbgEvent);

     inherited Destroy;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.CleanWorkSpace
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
Procedure TDebugWin.CleanWorkSpace;
(*******************************************************************************)
var
   i    : integer;
   th   : PThreadStruct;
   Pid  : PPIDStruct;
   pDll : PTDLLStruct ;
   bp   : PBreakPoint;
begin
	  if FTIDs <> nil then
    begin
        i := 0;
        while i < FTIDs.Count  do
        begin
            th := PThreadStruct(FTIDs.Items[i]);
            FTIDs.Delete(i);
            FreeMem(th);
            inc(i);
        end;

        FTIDs.Clear;
    end;

    if FPIDs <> nil then
    begin
        i := 0;
        while i < FPIDs.Count  do
        begin
            pid := PPIDStruct(FPIDs.Items[i]);
            SymCleanup( pid^.hProc);
            FPIDs.Delete(i);
            FreeMem(pid);
            inc(i);
        end;

        FPIDs.Clear;
    end;

    if FDLLs <> nil then
    begin
        i := 0;
        while i < FDLLs.Count  do
        begin
            pDll := PTDLLStruct(FDLLs.Items[i]);
            FDLLs.Delete(i);
            FreeMem(pDll);
            inc(i);
        end;

        FDLLs.Clear;
    end;

    if FBPXs <> nil then
    begin
        i := 0;
        while i < FBPXs.Count  do
        begin
            bp := PBreakPoint(FBPXs.Items[i]);
            if bp.dwHandle = 2 then
            begin
                 FBPXs.Delete(i);
                 FreeMem(bp);
            end;
            inc(i);
        end;

        FBPXs.Clear;
    end;

    if (FProcessInfo.hProcess <> 0) and(FProcessInfo.hThread <> 0) then
    begin
		     CloseHandle(FProcessInfo.hProcess);
		     CloseHandle(FProcessInfo.hThread);
		     ZeroMemory(@FStartupInfo, sizeof(FStartupInfo));
		     FStartupInfo.cb := sizeof(FStartupInfo);
		     ZeroMemory(@FProcessInfo, sizeof(FProcessInfo));
	  end;

	ZeroMemory(@Fm_dbgPI, sizeof(Fm_dbgPI));

end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.GetFileNameFromHandle
  Author:    Max
  DateTime:  2013.08.27
  Arguments: Handle: THandle
  Result:    String
-------------------------------------------------------------------------------}
function TDebugWin.GetFileNameFromHandle(Handle: THandle): String;
(*******************************************************************************)
var
  IO_STATUSBLOCK: IO_STATUS_BLOCK;
  FileNameInfo  : FILE_NAME_INFORMATION;
  szFile        : String;
begin
    FillChar(FileNameInfo.FileName, SizeOf(FileNameInfo.FileName), 0);
    NtQueryInformationFile(Handle, @IO_STATUSBLOCK, @FileNameInfo, 500, 9);
    szFile := WideCharToString(FileNameInfo.FileName);
    Result := szFile;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.SetDefaultSetting
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TDebugWin.SetDefaultSetting;
(*******************************************************************************)
var
  tmpSetting : TDbgSettings;
begin

    tmpSetting.bDebugChilds          := True;
    tmpSetting.bAutoLoadSymbols      := True;
    tmpSetting.bBreakOnNewDLL        := False;
	  tmpSetting.bBreakOnNewTID        := False;
	  tmpSetting.bBreakOnNewPID        := False;
	  tmpSetting.bBreakOnExDLL         := False;
	  tmpSetting.bBreakOnExTID         := False;
	  tmpSetting.bBreakOnExPID         := False;
	  tmpSetting.bBreakOnModuleEP      := True;
	  tmpSetting.bBreakOnSystemEP      := False;
	  tmpSetting.bBreakOnTLS           := False;
	  tmpSetting.bUseExceptionAssist   := False;
    tmpSetting.dwSuspendType         := 0;
    tmpSetting.dwDefaultExceptionMode:= HANDLE_BREAK;  // 0 = Break and wait for User *DEFAULT*

    tmpSetting.bNotWaitEventDbg      := True;
    //tmpSetting.dwBreakOnEPMode       := BREAK_ON_EP;   // 0 = default application EP


    FDbgSetting := tmpSetting;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.EnableDebugFlag
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.EnableDebugFlag: Boolean;
(*******************************************************************************)
var
  Pid          : DWORD;
  ReturnLength : DWORD;
  hProcess     : THandle;
  hToken       : THandle;
  TP           : WinApi.Windows.TOKEN_PRIVILEGES;
  TPprev       : WinApi.Windows.TOKEN_PRIVILEGES;
begin
     Result   := True;
     pid      := GetCurrentProcessID;
     hProcess := OpenProcess(PROCESS_ALL_ACCESS, True, pid);
     hToken := 0;

     if hProcess <> 0 then
     begin
          if OpenProcessToken(hProcess, TOKEN_QUERY or TOKEN_ADJUST_PRIVILEGES, hToken) then
          begin
               ZeroMemory(@TP, sizeof(TP));

               if lookupPrivilegeValue(nil, 'SeDebugPrivilege', TP.Privileges[0].Luid) then
               begin
                    TP.Privileges[0].Attributes := SE_PRIVILEGE_ENABLED;
                    TP.PrivilegeCount := 1;
                    if not AdjustTokenPrivileges(hToken, False, tp, sizeof(TP), TPprev, returnlength) then
                           Result := False;
               end;
          end;
     end;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.PBProcInfo
  Author:    Max
  DateTime:  2013.08.27
  Arguments: dwPID: DWORD; sFileName:string; dwEP,dwIBase: NativeUInt;dwExitCode: DWORD; hProc: THandle
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.PBProcInfo(dwPID: DWORD; sFileName:string; dwEP,dwIBase: NativeUInt;dwExitCode: DWORD; hProc: THandle):Boolean ;
(*******************************************************************************)
var
   bFound : Boolean;
   i      : Integer;
   newPID : PPIDStruct;
begin
	   bFound := false;

     if Assigned(FPIDs) then
     begin
           for i := 0 to  FPIDs.Count - 1  do
           begin
                if(PPIDStruct(FPIDs[i])^.dwPID = dwPID) then
                begin
                     PPIDStruct(FPIDs[i])^.dwExitCode := dwExitCode;
                     PPIDStruct(FPIDs[i])^.bRunning   := False;
                     bFound                           := True;
                end;
           end;
     end;

     if not bFound then
	   begin
          GetMem(newPID,SizeOf(TPIDStruct));
          ZeroMemory(newPID,SizeOf(TPIDStruct));

          newPID^.dwPID           := dwPID;
          newPID^.dwEP            := dwEP;
          newPID^.dwIBase         := dwIBase;
          newPID^.sFileName       := sFileName;
          newPID^.dwExitCode      := dwExitCode;
          newPID^.hProc           := hProc;
          if not FNormalDebugging then
          begin
               FNormalDebugging   := true;
			         newPID^.bKernelBP  := true;
          end
          else
               newPID^.bKernelBP  := False;

          newPID.bWOW64KernelBP   := False;
          newPID^.bSymLoad        := False;
          newPID^.bRunning        := True;
          newPID^.bTrapFlag       := False;
          newPID^.bTraceFlag      := False;
          newPID^.dwBPRestoreFlag := 0;
          FPIDs.Add(newPID);
	   end;

     if Assigned(FOnPID) then
     begin
          if  not bFound then  FOnPID(dwPID,sFileName,dwExitCode,dwEP,dwIBase,bFound)
          else                 FOnPID(dwPID,'',dwExitCode,dwEP,dwIBase,bFound)
     end;

     if bFound then FMsg := (PChar(Format(rsExitProcess,[dwPID,dwExitCode])))
     else           FMsg := (PChar(Format(rsNewProcess,[dwPID,dwEP])));

	   Synchronize(PBLogInfo);
     Result := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.PBThreadInfo
  Author:    Max
  DateTime:  2013.08.27
  Arguments: dwPID,dwTID: DWORD; dwEP: NativeUInt; bSuspended: Boolean; dwExitCode: DWORD; bNew: Boolean
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.PBThreadInfo(dwPID,dwTID: DWORD; dwEP: NativeUInt; bSuspended: Boolean; dwExitCode: DWORD; bNew: Boolean): Boolean;
(*******************************************************************************)
var
   bFound : Boolean;
   i      : Integer;
   newTID : PThreadStruct;
begin
	   bFound := false;

     if Assigned(FTIDs) then
     begin
         for i := 0 to  FTIDs.Count - 1  do
         begin
              if(PThreadStruct(FTIDs[i])^.dwTID = dwTID) and (PThreadStruct(FTIDs[i])^.dwPID = dwPID) then
              begin
                   PThreadStruct(FTIDs[i])^.dwExitCode := dwExitCode;
                   bFound                              := True;
              end;
         end;
     end;

     if  not bFound then
     begin
          GetMem(newTID,SizeOf(TThreadStruct));
          ZeroMemory(newTID,SizeOf(TThreadStruct));
          newTID^.bSuspended := bSuspended;
          newTID^.dwEP       := dwEP;
          newTID^.dwTID      := dwTID;
          newTID^.dwPID      := dwPID;
          newTID^.dwExitCode := dwExitCode;
          FTIDs.Add(newTID);
		 end;
     if Assigned(FOnThread) then
     begin
          OnThread(dwPID,dwTID,dwEP,bSuspended,dwExitCode,bFound);
     end;

     if bFound then FMsg := (PChar(Format(rsExitThread,[dwTID,dwPID,dwExitCode])))
     else           FMsg := (PChar(Format(rsNewThread,[dwTID,dwPID,dwEP])));

	   Synchronize(PBLogInfo);
     Result := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.PBExceptionInfo
  Author:    Max
  DateTime:  2013.08.27
  Arguments: dwExceptionOffset, dwExceptionCode: NativeUInt;dwPID, dwTID: DWORD
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.PBExceptionInfo(dwExceptionOffset, dwExceptionCode: NativeUInt;dwPID, dwTID: DWORD): Boolean;
(*******************************************************************************)
begin
     FMsg := (PChar(Format(rsExceptionInfo,[dwExceptionCode,dwExceptionOffset,dwPID,dwTID])));
     {clsHelperClass::LoadSymbolForAddr(sFuncName,sModName,dwExceptionOffset,GetCurrentProcessHandle(dwPID));

	    emit OnException(sFuncName,sModName,dwExceptionOffset,dwExceptionCode,dwPID,dwTID);}
	   Synchronize(PBLogInfo);
     Result := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.PBDLLInfo
  Author:    Max
  DateTime:  2013.08.27
  Arguments: sDLLPath: string; dwPID: DWORD; dwEP: NativeUInt;bLoaded: Boolean
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.PBDLLInfo(sDLLPath: string; dwPID: DWORD; dwEP: NativeUInt;bLoaded: Boolean): Boolean;
(*******************************************************************************)
var
  bFound : Boolean;
  i      : Integer;
  newDLL : PTDLLStruct;
begin
      if(sDLLPath = '')  then
      begin
          Result := False;
          Exit;
      end;

      bFound := False;
      if Assigned(FDLLs) then
      begin
          for i := 0 to  FDLLs.Count - 1  do
          begin
               if (PTDLLStruct(FDLLs)^.sPath = sDLLPath) and  (PTDLLStruct(FDLLs)^.dwPID = dwPID)then
               begin
                    PTDLLStruct(FDLLs)^.bLoaded := True;
                    bFound                      := True;
               end;
          end;
      end;

	    if not bFound then
	    begin
           GetMem(newDLL,SizeOf(TDLLStruct));
           ZeroMemory(newDLL,SizeOf(TDLLStruct));
           newDLL^.bLoaded   := bLoaded;
		       newDLL^.dwBaseAdr := dwEP;
		       newDLL^.sPath     := sDLLPath;
		       newDLL^.dwPID     := dwPID;
           FDLLs.Add(newDLL);
      end;

      if Assigned(FOnDll) then
      begin
           OnDll(sDLLPath,dwPID,dwEP,bLoaded);
      end;

      if bFound then FMsg := (PChar(Format(rsUnloadDLL,[dwPID,sDLLPath])))
      else           FMsg := (PChar(Format(rsLoadDLL,[dwPID,sDLLPath,NativeUInt(dwEP)])));

      Synchronize(PBLogInfo);
      Result := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.PBDbgString
  Author:    Max
  DateTime:  2013.08.27
  Arguments: sMessage: string;dwPID: DWORD
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.PBDbgString(sMessage: string;dwPID: DWORD): Boolean;
(*******************************************************************************)
begin
     if Assigned(FOnDbgString) then
     begin
          FOnDbgString(sMessage,dwPID);
     end;

	   Result := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.PBLogInfo
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TDebugWin.PBLogInfo;
(*******************************************************************************)
var
   vTime : TSystemTime;
begin
	  if Assigned(FOnLog) then
	  begin
         GetLocalTime(vtime);
         FTime := SystemTimeToDateTime(vTime);
         FOnLog(FTime,FMsg);
    end;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.DoOnDbgEnd
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TDebugWin.DoOnDbgEnd;
(*******************************************************************************)
begin
     FOnDebuggerEnd;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.DoDbgBreak
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TDebugWin.DoDbgBreak;
(*******************************************************************************)

begin
	  if Assigned(FOnDebuggerBreak) then
           FOnDebuggerBreak ;
end;

(************funzioni public*****************)
(********************************************)

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.DetachFromProcess
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.DetachFromProcess:Boolean;
(*******************************************************************************)
var
  i : Integer;
  DebugActiveProcessStop : TDebugActiveProcessStop;
  DebugBreakProcess      : TDebugBreakProcess;
begin
    FNormalDebugging := True;
    FbStopDebugging  := True;

    @DebugActiveProcessStop  := GetProcAddress(GetModuleHandleA('Kernel32.Dll'), 'DebugActiveProcessStop');
    @DebugBreakProcess       := GetProcAddress(GetModuleHandleA('Kernel32.Dll'), 'DebugBreakProcess') ;

    RemoveBPs;

     for i := 0 to  FPIDs.Count - 1  do
     begin
          if(not CheckProcessState(PPIDStruct(FPIDs[i])^.dwPID)) then Break;
          DebugBreakProcess(PPIDStruct(FPIDs[i])^.hProc);
          PulseEvent(FhDbgEvent);
     end;

     Result := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.AttachToProcess
  Author:    Max
  DateTime:  2013.08.27
  Arguments: dwPID: DWORD
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.AttachToProcess(dwPID: DWORD): Boolean;
(*******************************************************************************)
begin
	   CleanWorkSpace;
	   FNormalDebugging := false;
     FdwPidToAttach   := dwPID;
	   Result           := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.SuspendDebuggingAll
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.SuspendDebuggingAll: Boolean;
(*******************************************************************************)
var
   i : Integer;
begin
     for i := 0 to  FPIDs.Count - 1  do
      SuspendDebugging(PPIDStruct(FPIDs[i])^.dwPID);
    Result := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.SuspendDebugging
  Author:    Max
  DateTime:  2013.08.27
  Arguments: dwPID : DWORD
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.SuspendDebugging(dwPID : DWORD): Boolean;
(*******************************************************************************)
var
  DebugBreakProcess : TDebugBreakProcess;
  hProcess          : THandle;
  i                 : Integer;
begin
     @DebugBreakProcess := GetProcAddress(GetModuleHandleA('kernel32.dll'), 'DebugBreakProcess');
	   if(CheckProcessState(dwPID)) then
	   begin
		      if(FDbgSetting.dwSuspendType = 0) then
		      begin
			         hProcess := 0;
			         for i := 0 to  FPIDs.Count - 1  do
               begin
				             if(PPIDStruct(FPIDs[i])^.bRunning) and (PPIDStruct(FPIDs[i])^.dwPID = dwPID) then
					               hProcess := PPIDStruct(FPIDs[i])^.hProc;
			         end;

			         if(DebugBreakProcess(hProcess)) then
			         begin
                    FMsg := (PChar(Format(rsDebugSuspend,[dwPID])));
				            Synchronize(PBLogInfo);
				            Result := True;
                    Exit;
			         end;
          end else
		      begin
              if(SuspendProcess(dwPID,true)) then
              begin
                   FMsg := (PChar(Format(rsDebugSuspend,[dwPID])));
                   Synchronize(PBLogInfo);
                   Result := True;
                   Exit;
              end;
	        end;
     end;
	   Result := False;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.StopDebuggingAll
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.StopDebuggingAll:Boolean ;
(*******************************************************************************)
var
   i : Integer;
begin
     FIsDebug := False;
    for i := 0 to  FPIDs.Count - 1  do
      StopDebugging(PPIDStruct(FPIDs[i])^.dwPID);

    Result := PulseEvent(FhDbgEvent);
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.StopDebugging
  Author:    Max
  DateTime:  2013.08.27
  Arguments: dwPID : DWORD
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.StopDebugging(dwPID : DWORD) : Boolean;
(*******************************************************************************)
var
   hProcess: THandle;
   DebugActiveProcessStop : TDebugActiveProcessStop;
begin
     Result := True;

	   hProcess := GetCurrentProcessHandle(dwPID);
     @DebugActiveProcessStop  := GetProcAddress(GetModuleHandleA('Kernel32.Dll'), 'DebugActiveProcessStop');

     if hProcess <> 0 then
     begin
         if(CheckProcessState(dwPID)) then
         begin
             if @DebugActiveProcessStop <> nil then
               if DebugActiveProcessStop(dwPID) then Exit;

             if not TerminateProcess(hProcess,0) then Result := False;
         end;
         CloseHandle(hProcess);
     end
     else Result := False;

end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.ResumeDebugging
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.ResumeDebugging: Boolean;
(*******************************************************************************)
var
   i : Integer;
begin
     for i := 0 to  FPIDs.Count - 1  do
       SuspendProcess(PPIDStruct(FPIDs[i])^.dwPID,false);
    Result := PulseEvent(FhDbgEvent);
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.RestartDebugging
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.RestartDebugging: Boolean;
(*******************************************************************************)
begin
    StopDebuggingAll;
    Sleep(2000);
    StartDebugging;
    Result := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.GetDebuggingState
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.GetDebuggingState : Boolean;
(*******************************************************************************)
begin
     Result := FIsDebug
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.IsTargetSet
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.IsTargetSet: Boolean;
(*******************************************************************************)
begin
     Result := False;
	   if FszFileName <> '' then   Result := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.StepOver
  Author:    Max
  DateTime:  2013.08.27
  Arguments: dwNewOffset: DWORD; StepCallBack: Pointer
  Result:    Boolean
-------------------------------------------------------------------------------}
Function TDebugWin.StepOver(dwNewOffset: DWORD; StepCallBack: Pointer):Boolean;
(*******************************************************************************)
var
   i        : Integer;
   bpp,newBP: PBreakPoint;
begin
      i := 0;
      while i < FBPXs.Count  do
      begin
           bpp:=PBreakPoint(FBPXs.Items[i]);
           if bpp.dwHandle = $2 then
           begin
                dBPX(bpp.dwPID,bpp.bpxAddress,bpp.bpxSize,bpp.OriginalByte);
			          FBPXs.Delete(i);
		       	    FreeMem(bpp);
           end;
           inc(i);
      end;

      GetMem(newBP,SizeOf(BreakPoint));
      ZeroMemory(newBP,SizeOf(BreakPoint));

      newBP^.bpxAddress  := dwNewOffset;
      newBP^.dwHandle    := $2;
		  newBP^.bpxSize     := $1;
		  newBP^.dwPID       := FdwCurPID;
      newBP^.OriginalByte:= 0;
      newBP^.bpxCallBack := StepCallBack;
      newBP^.moduleName  := StrAlloc(MAX_PATH);
      ZeroMemory(newBP.moduleName,MAX_PATH * SizeOf(Char));

      SetBPX(newBP.dwPID,NativeUInt(newBP.bpxAddress),newBP.dwHandle,newBP.bpxSize,newBP^.OriginalByte) ;

      if  newBP^.OriginalByte <> $CC then
      begin
           FBPXs.Add(newbp);
           PulseEvent(FhDbgEvent);
           Result := True;
           Exit;
      end;
      Result := False;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.StepInto
  Author:    Max
  DateTime:  2013.08.27
  Arguments: StepCallBack: Pointer
  Result:    Boolean
-------------------------------------------------------------------------------}
Function TDebugWin.StepInto(StepCallBack: Pointer;bRemove: Boolean = False):Boolean;
(*******************************************************************************)
begin
      if bRemove then
      begin
           FStepCallBack    := nil;
           FbSingleStepFlag := False;
          // Result           := PulseEvent(FhDbgEvent);
      end else
      begin
           FStepCallBack    := StepCallBack;
           FbSingleStepFlag := True;
           FpCtx.EFlags     := FpCtx.EFlags or $100;
           Result           := PulseEvent(FhDbgEvent);
      end;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.IsDebuggerSuspended
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    Boolean
-------------------------------------------------------------------------------}
function TDebugWin.IsDebuggerSuspended: Boolean ;
(*******************************************************************************)
begin
	   Result := Fm_debuggerBreak;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.GetMainProcessID
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    DWORD
-------------------------------------------------------------------------------}
function TDebugWin.GetMainProcessID: DWORD;
(*******************************************************************************)
begin
	   Result := Fm_dbgPI.dwProcessId;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.GetMainThreadID
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    DWORD
-------------------------------------------------------------------------------}
function TDebugWin.GetMainThreadID: DWORD;
(*******************************************************************************)
begin
	   Result := Fm_dbgPI.dwThreadId;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.GetCurrentProcessHandle
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    THandle
-------------------------------------------------------------------------------}
function TDebugWin.GetCurrentProcessHandle: THandle;
(*******************************************************************************)
begin
     if IsDebuggerSuspended then  Result := FhCurProc
     else                         Result := GetProcessHandleByPID(Cardinal(-1));
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.GetCurrentPID
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    DWORD
-------------------------------------------------------------------------------}
function TDebugWin.GetCurrentPID: DWORD;
(*******************************************************************************)
begin
     if IsDebuggerSuspended then Result := FdwCurPID
     else                        Result := GetMainProcessID;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.GetCurrentTID
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    DWORD
-------------------------------------------------------------------------------}
function TDebugWin.GetCurrentTID: DWORD;
(*******************************************************************************)
begin
     if IsDebuggerSuspended then Result := FdwCurTID
     else                        Result := GetMainThreadID;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.ClearTarget
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    None
-------------------------------------------------------------------------------}
procedure TDebugWin.ClearTarget;
(*******************************************************************************)
begin
	   FszFileName := '';
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.SetTarget
  Author:    Max
  DateTime:  2013.08.27
  Arguments: sTarget: string
  Result:    None
-------------------------------------------------------------------------------}
procedure TDebugWin.SetTarget(sTarget: string);
(*******************************************************************************)
begin
	   FszFileName      := sTarget;
	   FNormalDebugging := True;
end;

{-------------------------------------------------------------------------------
  Procedure: TDebugWin.GetTarget
  Author:    Max
  DateTime:  2013.08.27
  Arguments: None
  Result:    string
-------------------------------------------------------------------------------}
function TDebugWin.GetTarget: string;
(*******************************************************************************)
begin
	   Result := FszFileName;
end;

procedure TDebugWin.ClearCommandLine;
(*******************************************************************************)
begin
	   FszCommandLine := '';
end;

procedure TDebugWin.SetCommandLine(CommandLine : string);
(*******************************************************************************)
begin
  	 FszCommandLine := CommandLine;
end;

function TDebugWin.GetCommandLine: string;
(*******************************************************************************)
begin
	   Result := FszCommandLine;
end;

procedure TDebugWin.Execute;
(*******************************************************************************)
begin

    if(FdwPidToAttach <> 0) and ( not FNormalDebugging)then
    begin
        CleanWorkSpace;
        FIsDebug         := true;
        FbSingleStepFlag := False;
        AttachedDebugging;
    end else
    begin
        if(FszFileName = '') or  (FIsDebug)  then  Exit;

        CleanWorkSpace;
        FIsDebug         := true;
        FbSingleStepFlag := False;
        NormalDebugging;
    end;

end;

function TDebugWin.StartDebugging: Boolean;
(*******************************************************************************)
begin
     Start;
     Result := True;
end;

procedure TDebugWin.AttachedDebugging;
(*******************************************************************************)
begin
    if(CheckProcessState(FdwPidToAttach)) and (DebugActiveProcess(FdwPidToAttach)) then
    begin
     // FhDbgEvent := CreateEvent(nil,False,False,'hDebugEvent');
      FMsg := (rsAttachProc);
      Synchronize(PBLogInfo);
      DebugLoop;
      FNormalDebugging := True;
      Exit;
    end;
    FIsDebug := False;
    if Assigned(FOnDebuggerEnd) then  Synchronize(DoOnDbgEnd);

end;

procedure TDebugWin.NormalDebugging ;
(*******************************************************************************)
var
   dwCreationFlags: DWORD;

begin

    //GetStartupInfo(dbgStartupInfo);
    dwCreationFlags := DEBUG_ONLY_THIS_PROCESS;

	  FhDbgEvent := CreateEvent(nil,False,False,'hDebugEvent');

  	if(FDbgSetting.bDebugChilds = True) then  dwCreationFlags := DEBUG_PROCESS;

    if CreateProcess(PChar(FszFileName),
                             PChar('"'+FszFileName+'" '+FszCommandLine),
                             nil,                                  //lpProcessAttributes
                             nil,                                  //lpThreadAttributes
                             false,                                //bInheritHandles
                             dwCreationFlags,
                             nil,                                  //lpEnvironment
                             PChar(ExtractFilePath(FszFileName)),  //lpCurrentDirectory
                             FStartupInfo,                         //lpStartupInfo
                             FProcessInfo                          //lpProcessInformation
                              )    then
   		 DebugLoop
    else begin
      FIsDebug := False;
      if Assigned(FOnDebuggerEnd) then Synchronize(DoOnDbgEnd);
    end;
end;

procedure TDebugWin.DebugLoop;
(*******************************************************************************)
var
   DebugSetProcessKillOnExit : TDebugSetProcessKillOnExit;
   DebugActiveProcessStop    : TDebugActiveProcessStop;
   bContinueDebugging        : Boolean;
	 dwContinueStatus          : DWORD;
begin

     @DebugSetProcessKillOnExit := GetProcAddress(GetModuleHandleA('Kernel32.Dll'),'DebugSetProcessKillOnExit');
     @DebugActiveProcessStop    := GetProcAddress(GetModuleHandleA('Kernel32.Dll'), 'DebugActiveProcessStop');
     DebugSetProcessKillOnExit(false);
     bContinueDebugging := True;
	   dwContinueStatus   := DBG_CONTINUE;

     while(bContinueDebugging) and (FIsDebug) do
     begin
          if not WaitForDebugEvent(FDebugEvent, INFINITE) then  bContinueDebugging := False;

          if (FbStopDebugging) then
		      begin
              FbStopDebugging := False;
              DebugActiveProcessStop(FDebugEvent.dwProcessId);
              ContinueDebugEvent(FDebugEvent.dwProcessId,FDebugEvent.dwThreadId,DBG_CONTINUE);
              break;
		      end;

          case FDebugEvent.dwDebugEventCode of
              CREATE_PROCESS_DEBUG_EVENT: dwContinueStatus := CreateProcessDebugEvent(FDebugEvent);
              EXIT_PROCESS_DEBUG_EVENT:   dwContinueStatus := ExitProcessDebugEvent(FDebugEvent,bContinueDebugging);
              CREATE_THREAD_DEBUG_EVENT:  dwContinueStatus := CreateThreadDebugEvent(FDebugEvent);
              EXIT_THREAD_DEBUG_EVENT:    dwContinueStatus := ExitThreadDebugEvent(FDebugEvent);
              LOAD_DLL_DEBUG_EVENT:       dwContinueStatus := LoadDLLDebugEvent(FDebugEvent);
              UNLOAD_DLL_DEBUG_EVENT:     dwContinueStatus := UnloadDLLDebugEvent(FDebugEvent);
              OUTPUT_DEBUG_STRING_EVENT:  dwContinueStatus := OutputDebugStringEvent(FDebugEvent);
              EXCEPTION_DEBUG_EVENT:      dwContinueStatus := HandleExceptionDebugEvent(FDebugEvent);
          end;
          ContinueDebugEvent(FDebugEvent.dwProcessId,FDebugEvent.dwThreadId,dwContinueStatus);
		      dwContinueStatus := DBG_CONTINUE;
     end;

     FIsDebug := False;
     FMsg     := (rsDebugEnd);
     Synchronize(PBLogInfo);
	   CleanWorkSpace();

     //DebugActiveProcessStop(FDebugEvent.dwProcessId);
     if Assigned(FOnDebuggerEnd) then  Synchronize(DoOnDbgEnd);
 end;

function TDebugWin.CreateProcessDebugEvent(debugEvent: TDEBUGEVENT):Cardinal;
(*******************************************************************************)
var
   hProc         : THandle;
   dwBase        : NativeUInt;
   tcDllFilepath : AnsiString;
   iPid,i        : Integer;
begin
     Result := DBG_CONTINUE;
		 hProc  := debugevent.CreateProcessInfo.hProcess;
     if Fm_dbgPI.hProcess = 0 then
     begin
					Fm_dbgPI.hProcess   := debugevent.CreateProcessInfo.hProcess;
					Fm_dbgPI.hThread    := debugevent.CreateProcessInfo.hThread;
					Fm_dbgPI.dwProcessId:= debugevent.dwProcessId;
					Fm_dbgPI.dwThreadId := debugevent.dwThreadId;
     end;

		 tcDllFilepath := AnsiString(GetFileNameFromHandle(debugevent.CreateProcessInfo.hFile));
     dwBase        := NativeUInt(debugevent.CreateProcessInfo.lpBaseOfImage);
		 PBProcInfo(debugevent.dwProcessId,string(tcDllFilepath),NativeUInt(debugevent.CreateProcessInfo.lpStartAddress),dwBase,Cardinal(-1),hProc);

     if Assigned(FOnNewPid) then FOnNewPid(string(tcDllFilepath) ,debugevent.dwProcessId);

     iPid  := 0;
     for i := 0 to  FPIDs.Count - 1  do
		 begin
					if (PPIDStruct(FPIDs[i])^.dwPID = debugEvent.dwProcessId) then
          begin
               iPid := i;
               Break;
          end;
		 end;

     PPIDStruct(FPIDs[iPid])^.bSymLoad := SymInitialize(hProc,nil,False); ;
     if  not PPIDStruct(FPIDs[i])^.bSymLoad then
		 begin
					FMsg := (PChar(Format(rsNotLoadSymbol,[debugevent.dwProcessId])));
          Synchronize(PBLogInfo);
		 end
		 else SymLoadModule(hProc,0,PAnsiChar(tcDllFilepath),nil,NativeUInt(debugevent.CreateProcessInfo.lpBaseOfImage),0);

     AddBreakpointToList(BP_SOFTBPX,2,dword(-1),NativeUInt(debugevent.CreateProcessInfo.lpStartAddress),0,2,FEPCallBack);

     if FDbgSetting.bBreakOnTLS then
		 begin
				 {	DWORD64 tlsCallback = clsPEManager::getTLSCallbackOffset((wstring)tcDllFilepath,debug_event.dwProcessId);
					if(tlsCallback > 0)
						AddBreakpointToList(NULL,2,-1,(quint64)debug_event.u.CreateProcessInfo.lpBaseOfImage + tlsCallback,NULL,0x2); }
     end;

     InitBP ;
     // Insert Main Thread to List
		 // PBThreadInfo(debugevent.dwProcessId,clsHelperClass::GetMainThread(debug_event.dwProcessId),(quint64)debug_event.u.CreateThread.lpStartAddress,false,0,true);

     if FDbgSetting.bBreakOnNewDLL then
         Result  := CallBreakDebugger(debugevent,HANDLE_BREAK);
     CloseHandle(debugevent.CreateProcessInfo.hFile);

end;

Function TDebugWin.CreateThreadDebugEvent(debugEvent: TDEBUGEVENT):Cardinal;
(*******************************************************************************)
begin
     Result := DBG_CONTINUE;
     PBThreadInfo(debugevent.dwProcessId,FDebugEvent.dwThreadId,NativeUInt(debugevent.CreateThread.lpStartAddress),False,Cardinal(-1),True);
     InitBP();

		 if FDbgSetting.bBreakOnNewTID then
				 Result := CallBreakDebugger(debugevent,HANDLE_BREAK);

end;

function TDebugWin.ExitThreadDebugEvent(debugevent: TDEBUGEVENT):Cardinal;
(*******************************************************************************)
begin
     Result := DBG_CONTINUE;
     PBThreadInfo(debugevent.dwProcessId,debugevent.dwThreadId,NativeUInt(debugevent.CreateThread.lpStartAddress),False,debugevent.ExitThread.dwExitCode,False);
     if FDbgSetting.bBreakOnExTID then
	 			Result := CallBreakDebugger(debugevent,HANDLE_BREAK);

end;

function TDebugWin.ExitProcessDebugEvent(debugEvent: TDEBUGEVENT; var bContinueDebugging : Boolean):Cardinal;
(*******************************************************************************)
var
  bStillOneRunning   : Boolean;
  i                  : Integer;
begin
     Result := DBG_CONTINUE;
     PBProcInfo(FDebugEvent.dwProcessId,'',NativeUInt(debugevent.CreateProcessInfo.lpStartAddress),NativeUInt(debugevent.CreateProcessInfo.lpBaseOfImage),debugevent.ExitProcess.dwExitCode,0);
     SymCleanup(debugevent.CreateProcessInfo.hProcess);
     //CloseFile(debugevent.dwProcessId);

     bStillOneRunning := False;

     for i := 0 to FPIDs.Count -1 do
     begin
         if(PPIDStruct(FPIDs[i])^.bRunning) and (debugEvent.dwProcessId <> PPIDStruct(FPIDs[i])^.dwPID) then
         begin
              bStillOneRunning := True;
						  break;
         end;
     end;

		 if  not bStillOneRunning then bContinueDebugging := False;

     if FDbgSetting.bBreakOnExPID then
		 			Result := CallBreakDebugger(debugevent,HANDLE_BREAK);
end;

function TDebugWin.LoadDLLDebugEvent(debugEvent: TDEBUGEVENT): Cardinal;
(*******************************************************************************)
var
   sDLLFileName : AnsiString;
   hProcess     : THandle;
   i ,iPid      : Integer;

begin
     Result   := DBG_CONTINUE;
     hProcess := 0;
     iPid     := 0;

     sDLLFileName := AnsiString( GetFileNameFromHandle(debugevent.LoadDll.hFile) );
     PBDLLInfo(string(sDLLFileName),debugEvent.dwProcessId,NativeUInt(debugEvent.LoadDll.lpBaseOfDll),true);

     for i := 0 to  FPIDs.Count - 1  do
		 begin
					if (PPIDStruct(FPIDs[i])^.dwPID = debugEvent.dwProcessId) then
          begin
               hProcess := PPIDStruct(FPIDs[i])^.hProc;
               iPid     := i;
          end;
		 end;
     if(PPIDStruct(FPIDs[iPid])^.bSymLoad) and (FDbgSetting.bAutoLoadSymbols) then
				  SymLoadModule(hProcess,0,PAnsiChar(sDLLFileName),nil,NativeUInt(debugevent.CreateProcessInfo.lpBaseOfImage),0)
		 else
		 begin
          FMsg := (PChar(Format(rsNotLoadSymbolDLL,[sDLLFileName])));
          Synchronize(PBLogInfo);
		 end;

     if FDbgSetting.bBreakOnNewDLL then
			 	 Result := CallBreakDebugger(debugevent,HANDLE_BREAK);

     CloseHandle(debugevent.LoadDll.hFile);

end;

function TDebugWin.UnloadDLLDebugEvent(debugEvent: TDEBUGEVENT): Cardinal;
(*******************************************************************************)
var
    i : Integer;
begin
     Result := DBG_CONTINUE;
     for i := 0 to  FDLLs.Count - 1  do
     begin
           if (PTDLLStruct(FDLLs)^.dwBaseAdr = NativeUInt(debugEvent.UnloadDll.lpBaseOfDll)) and  (PTDLLStruct(FDLLs)^.dwPID = debugEvent.dwProcessId)then
           begin
                PBDLLInfo(PTDLLStruct(FDLLs)^.sPath,PTDLLStruct(FDLLs)^.dwPID,PTDLLStruct(FDLLs)^.dwBaseAdr,False);
                SymUnloadModule(GetCurrentProcessHandle(PTDLLStruct(FDLLs)^.dwPID),PTDLLStruct(FDLLs)^.dwBaseAdr);
                Break;
           end;
     end;

     if FdbgSetting.bBreakOnExDLL then
	 			Result := CallBreakDebugger(debugevent,HANDLE_BREAK);

end;

function TDebugWin.OutputDebugStringEvent(debugEvent: TDEBUGEVENT): Cardinal;
(*******************************************************************************)
var
  i        : Integer;
  hProcess : THandle;
  ws       : pwidechar;
  s        : PChar;
  x        : NativeUInt;
  DbgString: string;

begin
     hProcess:= 0;
     for i := 0 to FPIDs.Count -1 do
		 begin
		      if(debugEvent.dwProcessId = PPIDStruct(FPIDs[i])^.dwPID)  then
		         hProcess := PPIDStruct(FPIDs[i])^.hProc;
		 end;

     if debugEvent.DebugString.fUnicode>0 then
     begin
          GetMem(ws,debugEvent.DebugString.nDebugStringLength + $2);
          try
            ReadProcessMemory(hProcess, debugEvent.DebugString.lpDebugStringData, ws, debugEvent.DebugString.nDebugStringLength,x);
            ws[debugEvent.DebugString.nDebugStringLength div 2]:=#0;
            ws[x div 2]:=#0;
            DbgString := ws
          finally
            freemem(ws);
          end;
     end else
     begin
          GetMem(s,debugEvent.DebugString.nDebugStringLength+1);
          try
            ReadProcessMemory(hProcess, debugEvent.DebugString.lpDebugStringData, s, debugEvent.DebugString.nDebugStringLength,x);
            s[debugEvent.DebugString.nDebugStringLength]:=#0;
            s[x]:=#0;
            DbgString := s;
          finally
            freemem(s);
          end;
     end;
     PBDbgString(DbgString,debugEvent.dwProcessId);
     Result := DBG_CONTINUE;
end;

function TDebugWin.HandleExceptionDebugEvent(debugEvent: TDEBUGEVENT): Cardinal;
(*******************************************************************************)
var
  i                 : integer;
  nB                : NativeUInt;
  bpp               : PBreakPoint;
  custEx            : PCustomException;
  exInfo            : EXCEPTION_DEBUG_INFO;
  iPid              : Integer;
  bIsEP, bIsBP,
  bIsKernelBP,
  bExceptionHandler,
  bStepOver         : Boolean;
  hProc             : THandle;


  MBI : WinApi.Windows.MEMORY_BASIC_INFORMATION;
  dwAddress,pageBase, pageSize : NativeUInt;

begin
    iPid       := 0;
    bIsEP      := False;
    bIsBP      := False;
    bIsKernelBP:= False;
    exInfo     := debugEvent.Exception;
    Result     := DBG_CONTINUE;
    bpp        := nil;

    for i := 0 to  FPIDs.Count - 1  do
		begin
					if (PPIDStruct(FPIDs[i])^.dwPID = debugEvent.dwProcessId) then iPid := i;
		end;

    if not CheckIfExceptionIsBP(NativeUInt(exInfo.ExceptionRecord.ExceptionAddress),exInfo.ExceptionRecord.ExceptionCode,debugEvent.dwProcessId,False) then
					PBExceptionInfo(NativeUInt(exInfo.ExceptionRecord.ExceptionAddress),exInfo.ExceptionRecord.ExceptionCode,debugEvent.dwProcessId,debugEvent.dwThreadId);

    case NTSTATUS(exInfo.ExceptionRecord.ExceptionCode) of
      STATUS_WX86_BREAKPOINT: begin  // Breakpoint in x86 Process which got executed in a x64 environment
              if (PPIDStruct(FPIDs[iPid])^.bKernelBP) and  (not  PPIDStruct(FPIDs[iPid])^.bWOW64KernelBP)  then
					    begin
                  if FdbgSetting.bBreakOnSystemEP then  Result := CallBreakDebugger(debugevent,HANDLE_BREAK)
                  else                    							Result := CallBreakDebugger(debugevent,HANDLE_J_STACK);

                  PPIDStruct(FPIDs[iPid])^.bWOW64KernelBP := true;
                  bIsKernelBP                             := true;
					    end;
      end;
      STATUS_BREAKPOINT: begin
              bStepOver  := False;
              // first SystemEP
              if( not PPIDStruct(FPIDs[iPid])^.bKernelBP) {and (FDbgSetting.dwBreakOnEPMode =  BREAK_ON_SYSTEM_EP)} then
						  begin
                   if FdbgSetting.bBreakOnSystemEP then  Result := CallBreakDebugger(debugevent,HANDLE_BREAK)
                   else                    						   Result := CallBreakDebugger(debugevent,HANDLE_J_STACK);

							     PPIDStruct(FPIDs[iPid])^.bKernelBP := True;
							     bIsKernelBP                        := True;
						  end
              else if PPIDStruct(FPIDs[iPid])^.bKernelBP then
              begin
                   if(UInt64(exInfo.ExceptionRecord.ExceptionAddress) = PPIDStruct(FPIDs[iPid])^.dwEP) then
							     begin
                       bIsEP := true;

                       UpdateOffsetsBPs;
                       InitBP;
							     end;

                   bIsBP := CheckIfExceptionIsBP(NativeUInt(exInfo.ExceptionRecord.ExceptionAddress),exInfo.ExceptionRecord.ExceptionCode,debugEvent.dwProcessId,True);
                   if bIsBP then
                   begin
                        hProc := PPIDStruct(FPIDs[iPid])^.hProc;

                        for i := 0 to FBPXs.Count - 1 do
                        begin
                             bpp:=PBreakPoint(FBPXs.Items[i]);
                             if(NativeUInt(exInfo.ExceptionRecord.ExceptionAddress) = bpp^.bpxAddress) and
										             ((bpp^.dwPID = debugEvent.dwProcessId) or (bpp^.dwPID = Cardinal(-1) )) then
									           begin
										              WriteProcessMemory(hProc,Pointer(bpp^.bpxAddress),@bpp^.OriginalByte, bpp^.bpxSize, nB);
										              FlushInstructionCache(hProc,Pointer(bpp^.bpxAddress),bpp^.bpxSize);

                                  if  bpp^.dwHandle <> $2 then
                                      bpp^.bRestoreBP := true
                                  else
                                      if  not bIsEP then bStepOver := true;
									           end;
                        end;

                        SetThreadContextHelper(True,True,debugEvent.dwThreadId,debugEvent.dwProcessId);
								        PPIDStruct(FPIDs[iPid])^.bTrapFlag       := True;
								        PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag := $2;

                        if (bIsEP) and ( not FDbgSetting.bBreakOnModuleEP) then
                        begin
                             if bpp.bpxCallBack <> nil then
                             begin
                                  @CallBackCustomBreakPoint :=  bpp.bpxCallBack;
                                  CallBackCustomBreakPoint;
                             end;
									           Result := CallBreakDebugger(debugEvent,HANDLE_IGNORE) ;
                        end else
                        begin
                             if not bStepOver then
                             begin
                                  if(bIsEP) then  FMsg := (PChar(Format(rsBreakOnEP,[debugevent.dwProcessId,NativeUInt(exInfo.ExceptionRecord.ExceptionAddress)])))
                                  else            FMsg := (PChar(Format(rsBreakOnSoftwareBP,[debugevent.dwProcessId,NativeUInt(exInfo.ExceptionRecord.ExceptionAddress)]))) ;
                                  Synchronize(PBLogInfo);
                             end;
                             Result :=  CallBreakDebugger(debugEvent,HANDLE_BREAK, bpp.bpxCallBack);
                        end
                   end;
              end;
      end;
      STATUS_WX86_SINGLE_STEP,
      STATUS_SINGLE_STEP :  begin
              if (PPIDStruct(FPIDs[iPid])^.bTraceFlag) and (FbSingleStepFlag) then
						  begin
							     bIsBP := true;
							     SetThreadContextHelper(false,true,debugevent.dwThreadId,debugevent.dwProcessId);
							    //qtDLGTrace::addTraceData((quint64)debug_event.u.Exception.ExceptionRecord.ExceptionAddress,debug_event.dwProcessId,debug_event.dwThreadId);
              end else
              begin
                  bIsBP := CheckIfExceptionIsBP(NativeUInt(exInfo.ExceptionRecord.ExceptionAddress),exInfo.ExceptionRecord.ExceptionCode,debugEvent.dwProcessId,True);
                  if bIsBP then
                  begin
                       if(PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag = $2)  then // Restore SoftwareBP
                       begin
                            for i := 0 to FBPXs.Count - 1 do
                            begin
                                  bpp:=PBreakPoint(FBPXs.Items[i]);
                                  if (bpp.dwHandle = $1) and (bpp.bRestoreBP) and ((bpp.dwPID = debugEvent.dwProcessId) or (bpp.dwPID = Cardinal(-1))) then
                                  begin
                                        SetBPX(bpp.dwPID,bpp.bpxAddress,bpp.dwHandle,bpp.bpxSize,bpp^.OriginalByte);
                                        bpp^.bRestoreBP := False;
                                        FMsg := (PChar(Format(rsRestoreBP,[debugevent.dwProcessId,bpp.bpxAddress])));
                                        Synchronize(PBLogInfo);
                                  end;
                            end;
                            PPIDStruct(FPIDs[iPid])^.bTrapFlag       := False;
                            PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag := 0;
                       end
                       else if(PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag = $4)  then // Restore MemBP
                       begin
                            for i := 0 to FBPMs.Count - 1 do
                            begin
                                  bpp:=PBreakPoint(FBPMs.Items[i]);
                                  if (bpp.dwHandle = $1) and (bpp.bRestoreBP) and ((bpp.dwPID = debugEvent.dwProcessId) or (bpp.dwPID = Cardinal(-1))) then
                                  begin
                                        SetMemoryBP(bpp.dwPID,bpp.bpxAddress,bpp.bpxSize,bpp.dwHandle);
                                        bpp^.bRestoreBP := False;
                                  end;
                            end;
                            PPIDStruct(FPIDs[iPid])^.bTrapFlag       := False;
                            PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag := 0;
                       end
                       else if(PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag = $8)  then // Restore HwBp
                       begin
                            for i := 0 to FBPHs.Count - 1 do
                            begin
                                  bpp:=PBreakPoint(FBPHs.Items[i]);
                                  if (bpp.dwHandle = $1) and (bpp.bRestoreBP) and ((bpp.dwPID = debugEvent.dwProcessId) or (bpp.dwPID = Cardinal(-1))) then
                                  begin
                                        SetHardwareBreakPoint(bpp.dwPID,bpp.bpxAddress,bpp.bpxSize,bpp.bpxSlot,bpp.bpxType);
                                        bpp^.bRestoreBP := False;
                                  end;
                            end;
                            PPIDStruct(FPIDs[iPid])^.bTrapFlag       := False;
                            PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag := 0;
                       end
                       else if(PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag = $0)  then // First time hit HwBP
                       begin
                            for i := 0 to FBPHs.Count - 1 do
                            begin
                                  bpp:=PBreakPoint(FBPHs.Items[i]);
                                  if (bpp.bpxAddress = NativeUInt(debugEvent.Exception.ExceptionRecord.ExceptionAddress)) and
                                     ( (bpp.dwPID = debugEvent.dwProcessId) or (bpp.dwPID = Cardinal(-1)) ) then
                                  begin
                                       if dHardwareBP(debugEvent.dwProcessId,bpp.bpxAddress,bpp.bpxSlot) then
                                       begin
                                            FMsg := (PChar(Format(rsBreakOnHBP,[debugevent.dwProcessId,bpp.bpxAddress])));
                                            Synchronize(PBLogInfo);
                                            Result                                   := CallBreakDebugger(debugEvent,HANDLE_BREAK,bpp.bpxCallBack);
                                            bpp^.bRestoreBP                          := True;
                                            PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag := $8;
                                            PPIDStruct(FPIDs[iPid])^.bTrapFlag       := True;

                                            SetThreadContextHelper(False,True,debugEvent.dwThreadId,debugEvent.dwProcessId);
                                       end;
                                  end;
                            end;
                            PPIDStruct(FPIDs[iPid])^.bTrapFlag       := False;
                            PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag := 0;
                       end else
                       begin
                            Result := Cardinal(DBG_EXCEPTION_NOT_HANDLED);
                       end;
                       if FbSingleStepFlag then
                       begin
                            FbSingleStepFlag := False;
                            bIsBP            := True;
                            Result := CallBreakDebugger(debugEvent,HANDLE_BREAK,FStepCallBack);
                       end;
                  end else
                  begin
                       if FbSingleStepFlag then
                       begin
                            FbSingleStepFlag := False;
                            bIsBP            := True;
                            Result  := CallBreakDebugger(debugEvent,HANDLE_BREAK,FStepCallBack);
                       end;
                  end;
              end;
      end;
      STATUS_GUARD_PAGE_VIOLATION: begin
              bIsBP := CheckIfExceptionIsBP(NativeUInt(exInfo.ExceptionRecord.ExceptionAddress),exInfo.ExceptionRecord.ExceptionCode,debugEvent.dwProcessId,True);
              if(bIsBP) then
						  begin
							     SetThreadContextHelper(false,true,debugEvent.dwThreadId,debugEvent.dwProcessId);
							     PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag := $4;
							     PPIDStruct(FPIDs[iPid])^.bTrapFlag       := True;

							     for i := 0 to FBPMs.Count - 1 do
                   begin
                        bpp := PBreakPoint(FBPMs.Items[i]);
							        	if( bpp.bpxAddress = NativeUInt(exInfo.ExceptionRecord.ExceptionAddress) ) then
                        begin
                             bpp^.bRestoreBP := True;
                             FMsg := (PChar(Format(rsBreakOnMemoryBP,[debugevent.dwProcessId,bpp.bpxAddress])));
                             Synchronize(PBLogInfo);
                             Result := CallBreakDebugger(debugEvent,HANDLE_BREAK,bpp.bpxCallBack);
                        end;
                   end;
              end  else
              begin
                   dwAddress := 0;
                   pageBase  := 0;
                   pageSize  := 0;
                   hProc := GetProcessHandleByPID(debugevent.dwProcessId);

                   While (VirtualQueryEx(hProc, Pointer(dwAddress), MBI, SizeOf(MEMORY_BASIC_INFORMATION)) > 0) do
                   begin
                        if(NativeUInt(exInfo.ExceptionRecord.ExceptionAddress) >= NativeUInt(MBI.BaseAddress)) and
                          (NativeUInt(exInfo.ExceptionRecord.ExceptionAddress) <= (NativeUInt(mbi.BaseAddress) + mbi.RegionSize) ) then
								        begin
								           	 pageSize := mbi.RegionSize;
									           pageBase := NativeUInt(mbi.BaseAddress);
                             Break;
								        end;
                        dwAddress := dwAddress + mbi.RegionSize;
                   end;

                   for i := 0 to FBPMs.Count - 1 do
                   begin
                        if (PBreakPoint(FBPMs[i])^.bpxAddress <= (pageBase + pageSize)) and
                           (PBreakPoint(FBPMs[i])^.bpxAddress >= pageBase) then
                        begin
                             bIsBP := true;
                             Break;
                        end;
                   end;
                   if(bIsBP) then
							     begin
								        SetThreadContextHelper(false,true,debugevent.dwThreadId,debugevent.dwProcessId);
								        PPIDStruct(FPIDs[iPid])^.dwBPRestoreFlag := $4;
								        PPIDStruct(FPIDs[iPid])^.bTrapFlag       := true;

								        Result := CallBreakDebugger(debugevent,HANDLE_IGNORE);
							     end
							     else Result := CallBreakDebugger(debugevent,HANDLE_BREAK);
              end;
      end;
    end;
    bExceptionHandler := False;
    if ( bIsBP = False) and ( bIsEP = False) and (bIsKernelBP = False) then
    begin
         for  i := 0 to  FExceptHandler.Count - 1 do
         begin
              custEx := PCustomException(FExceptHandler.Items[i]);
              if (custEx.dwExceptionType = debugEvent.Exception.ExceptionRecord.ExceptionCode) then
              begin
                   bExceptionHandler := True;
                   if custEx.dwHandler <> nil then
                   begin
                        @CustomExceptEvent :=  custEx.dwHandler;
                        Result             :=  CustomExceptEvent(debugEvent);

								        if(custEx.dwAction = 0)then
									        CallBreakDebugger(debugEvent,custEx.dwAction);
                   end
                   else Result := CallBreakDebugger(debugEvent,custEx.dwAction);
              end;
         end;
    end;
    if ( bExceptionHandler = False) and ( bIsBP = False) and ( bIsEP = False) and (bIsKernelBP = False) then
    begin
         if FdbgSetting.dwDefaultExceptionMode = 1 then
            Result := Cardinal(DBG_EXCEPTION_NOT_HANDLED)
         else if FdbgSetting.bUseExceptionAssist  then
         begin
              Fm_continueWithException := 0;
						  //emit AskForException((DWORD)debug_event.u.Exception.ExceptionRecord.ExceptionCode);
						  WaitForSingleObject(Fm_waitForGUI,INFINITE);

						  if Fm_continueWithException >= 10 then
              begin
							     Fm_continueWithException := Fm_continueWithException - 10;
							     CustomExceptionAdd(debugevent.Exception.ExceptionRecord.ExceptionCode,Fm_continueWithException,nil);
						  End;

						  Result := CallBreakDebugger(debugevent,Fm_continueWithException);
         end else
         begin
              Result := CallBreakDebugger(debugEvent,HANDLE_BREAK);
         end;
    end;
end;

(* dwHandle*
	|	0 = HANDLE_BREAK  - Break and wait for User *DEFAULT*
	|	1 = HANDLE_PASS   - pass to App/Os
	|	2 = HANDLE_IGNORE - Ignore
	|	3 = HANDLE_J_STACK- automatic workaround ( jumping to the return addr which is on the stack )
	*)
function TDebugWin.CallBreakDebugger(debugEvent: TDEBUGEVENT; dwHandle: DWORD;CallBack: Pointer = nil): DWORD;
(*******************************************************************************)
var
  hThread : THandle;
begin

    case dwHandle of
        HANDLE_BREAK:
        begin
              hThread             := OpenThread(THREAD_GETSET_CONTEXT,False,debugEvent.dwThreadId);
              FdwCurPID           := debugEvent.dwProcessId;
              FdwCurTID           := debugEvent.dwThreadId;
              FhCurProc           := GetCurrentProcessHandle(debugEvent.dwProcessId);
              Fm_debuggerBreak    := True;

              FpCtx^.ContextFlags := CONTEXT_ALL;
              GetThreadContext(hThread,FpCtx^);
              Synchronize(DoDbgBreak);

              if Assigned(CallBack) then
              begin
                   @CallBackCustomBreakPoint :=  CallBack;
                   CallBackCustomBreakPoint;
              end;

              if not FDbgSetting.bNotWaitEventDbg then
                  WaitForSingleObject(FhDbgEvent,INFINITE);
              SetThreadContext(hThread,FpCtx^);

              FdwCurPID := 0;FdwCurTID := 0;FhCurProc := 0;
              Fm_DebuggerBreak    := False;

              CloseHandle(hThread);
              Result := DBG_EXCEPTION_HANDLED;
        end;
        HANDLE_PASS   : Result := Cardinal(DBG_EXCEPTION_NOT_HANDLED);
        HANDLE_IGNORE : Result := DBG_CONTINUE;
        HANDLE_J_STACK: Result := DBG_EXCEPTION_HANDLED;
    else
        Result := Cardinal(DBG_EXCEPTION_NOT_HANDLED);
    end;
end;

function TDebugWin.CheckProcessState(dwPID : DWORD): Boolean;
(*******************************************************************************)
var
	hProcessSnap : THandle;
	procEntry32  : TProcessEntry32;
begin
      hProcessSnap := CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS,0);

      if(hProcessSnap = INVALID_HANDLE_VALUE) then
      begin
           Result := False;
           Exit;
      end;

      procEntry32.dwSize := sizeof(PROCESSENTRY32);

      if( not Process32First(hProcessSnap, procEntry32)) then
      begin
        CloseHandle(hProcessSnap);
        Result := False;
        Exit;
      end;

      while(Process32Next(hProcessSnap,&procEntry32)) do
      begin
           if(procEntry32.th32ProcessID = dwPID) then
           begin
                Result := True;
                Exit;
           end;
      end;
      CloseHandle(hProcessSnap);
      Result := False;
end;

function TDebugWin.CheckIfExceptionIsBP(dwExceptionOffset,dwExceptionType:NativeUInt; dwPID:DWORD; bClearTrapFlag:Boolean): Boolean;
(*******************************************************************************)
var
  i,iPID : Integer;
begin
     iPID  := 0;
     for i := 0 to  FPIDs.Count - 1  do
		 begin
					if (PPIDStruct(FPIDs[i])^.dwPID = dwPID) then iPid := i;
		 end;

	   if PPIDStruct(FPIDs[iPID])^.bTrapFlag then
	   begin
		    if bClearTrapFlag then PPIDStruct(FPIDs[iPID])^.bTrapFlag := False;
		    Result := True;
        Exit;
	   end;

     if ((dwExceptionType = EXCEPTION_SINGLE_STEP) or  (dwExceptionType = NativeUInt(STATUS_WX86_SINGLE_STEP)) ) and FbSingleStepFlag then
     begin
          Result := True;
          Exit;
     end;

     if (dwExceptionType = EXCEPTION_BREAKPOINT) or (dwExceptionType = NativeUInt(STATUS_WX86_BREAKPOINT)) or
		    (dwExceptionType = EXCEPTION_GUARD_PAGE) or (dwExceptionType = EXCEPTION_SINGLE_STEP) then
     begin
         for i := 0 to FBPXs.Count - 1 do
         begin
              if (PBreakPoint(FBPXs[i])^.bpxAddress = dwExceptionOffset) and
                 ((PBreakPoint(FBPXs[i])^.dwPID = dwPID) or (PBreakPoint(FBPXs[i])^.dwPID = Cardinal(-1)))  then
              begin
                 Result := True;
                 Exit;
              end;
         end;
         for i := 0 to FBPMs.Count - 1 do
         begin
              if (PBreakPoint(FBPMs[i])^.bpxAddress = dwExceptionOffset) and
                 ((PBreakPoint(FBPMs[i])^.dwPID = dwPID) or (PBreakPoint(FBPMs[i])^.dwPID = Cardinal(-1)))  then
              begin
                 Result := True;
                 Exit;
              end;
         end;
         for i := 0 to FBPHs.Count - 1 do
         begin
              if (PBreakPoint(FBPHs[i])^.bpxAddress = dwExceptionOffset) and
                 ((PBreakPoint(FBPHs[i])^.dwPID = dwPID) or (PBreakPoint(FBPHs[i])^.dwPID = Cardinal(-1)))  then
              begin
                 Result := True;
                 Exit;
              end;
         end;
     end;
     Result := False;
end;

function TDebugWin.SuspendProcess(dwPID : DWORD;bSuspend : Boolean): Boolean;
(*******************************************************************************)
var
  hProcessSnap : THandle;
	threadEntry32: TTHREADENTRY32;
  hThread      : THandle;
begin
	  threadEntry32.dwSize := sizeof(THREADENTRY32);

    hProcessSnap := CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD,dwPID);

    if(hProcessSnap = INVALID_HANDLE_VALUE) then
    begin
         Result := False;
         Exit;
    end;

    if( not Thread32First(hProcessSnap, threadEntry32)) then
    begin
         CloseHandle(hProcessSnap);
         Result := False;
         Exit;
    end;

    while Thread32Next(hProcessSnap, threadEntry32) do
    begin
         hThread := INVALID_HANDLE_VALUE;
         if(dwPID = threadEntry32.th32OwnerProcessID)  then
              hThread := OpenThread(THREAD_ALL_ACCESS,false,threadEntry32.th32ThreadID);

         if(hThread <> INVALID_HANDLE_VALUE)  then
         begin
              if  bSuspend then SuspendThread(hThread)
              else         			ResumeThread(hThread);
         end;
    end;
    Result := True;
end;

procedure TDebugWin.CustomExceptionAdd(dwExceptionType,dwAction: DWORD; pHandler:Pointer) ;
(*******************************************************************************)
var
  custEx : PCustomException;
begin
	  if(dwExceptionType <> Cardinal(STATUS_SINGLE_STEP)) and (dwExceptionType <> EXCEPTION_BREAKPOINT) then
	  begin
         GetMem(custEx,SizeOf(TCustomException));
         ZeroMemory(custEx,SizeOf(TCustomException));

        custEx^.dwAction        := dwAction;
        custEx^.dwExceptionType := dwExceptionType;
        custEx^.dwHandler       := pHandler;
        FExceptHandler.Add(custEx);
    end;
end;

procedure TDebugWin.CustomExceptionRemove(dwExceptionType: DWORD) ;
(*******************************************************************************)
var
  i      : Integer;
  custEx : PCustomException;
begin
      i := 0;
      while i < FExceptHandler.Count  do
      begin
           custEx := PCustomException(FExceptHandler.Items[i]);
           if (custEx.dwExceptionType = dwExceptionType) then
           begin
                FExceptHandler.Delete(i);
                FreeMem(custEx);
           end;
           inc(i);
      end;
end;

procedure TDebugWin.CustomExceptionRemoveAll;
(*******************************************************************************)
var
  i      : Integer;
  custEx : PCustomException;
begin
      i := 0;
      while i < FExceptHandler.Count  do
      begin
           custEx := PCustomException(FExceptHandler.Items[i]);
           FExceptHandler.Delete(i);
           FreeMem(custEx);
           inc(i);
      end;
	    FExceptHandler.Clear;
end;

function TDebugWin.SetThreadContextHelper(bDecIP,bSetTrapFlag: Boolean; dwThreadID, dwPID: DWORD): Boolean;
(*******************************************************************************)
var
   hThread  : THandle;
   cTT      : WinApi.Windows.TContext;
begin
     Result := False;

	   hThread := OpenThread(THREAD_GETSET_CONTEXT,false,dwThreadID);
	   if(hThread = INVALID_HANDLE_VALUE) then Exit;

     cTT.ContextFlags := CONTEXT_ALL;
	   GetThreadContext(hThread,cTT);
{$IF DEFINED(CPUX86)}
	   if bDecIP then cTT.Eip := cTT.Eip - 1;
{$ELSEIF DEFINED(CPUX64)}
     if bDecIP then cTT.Rip := cTT.Rip - 1;
{$ENDIF}
   	 if bSetTrapFlag then cTT.EFlags := cTT.EFlags or $100;

	   SetThreadContext(hThread,cTT);
     CloseHandle(hThread);
	   Result := True;
end;

function TDebugWin.GetCurrentProcessHandle(dwPID:DWORD): THandle;
(*******************************************************************************)
var
   i : Integer;
begin
     for i := 0 to  FPIDs.Count - 1  do
		 begin
					if (PPIDStruct(FPIDs[i])^.dwPID = dwPID) then
          begin
               Result :=  PPIDStruct(FPIDs[i])^.hProc;
               Exit;
          end;
		 end;
	   Result := Fm_dbgPI.hProcess;
end;

function TDebugWin.GetProcessHandleByPID(dwPID:DWORD): THandle;
(*******************************************************************************)
begin
     Result := GetCurrentProcessHandle(dwPID);
end;

function TDebugWin.IsOffsetAnBP(dwOffset: NativeUInt): Boolean;
(*******************************************************************************)
var
  i   : integer;
  bpp : PBreakPoint;
begin
     Result := False;
     for i := 0 to FBPXs.Count - 1 do
     begin
          bpp:=PBreakPoint(FBPXs.Items[i]);
          if (bpp.bpxAddress = dwOffset) then
          begin
               Result := True;
               Exit;
          end;
     end;

     for i := 0 to FBPMs.Count - 1 do
     begin
          bpp:=PBreakPoint(FBPMs.Items[i]);
          if (bpp.bpxAddress = dwOffset) then
          begin
               Result := True;
               Exit;
          end;
     end;

     for i := 0 to FBPHs.Count - 1 do
     begin
          bpp:=PBreakPoint(FBPHs.Items[i]);
          if (bpp.bpxAddress = dwOffset) then
          begin
               Result := True;
               Exit;
          end;
     end;
end;

function TDebugWin.IsOffsetAnEP(dwOffset: NativeUInt): Boolean;
(*******************************************************************************)
begin
     Result := False;
{$IF DEFINED(CPUX86)}
     if FpCtx.Eip = dwOffset then
     begin
          Result := True;
     end;
{$ELSEIF DEFINED(CPUX64)}
     if FpCtx.Rip= dwOffset then
     begin
          Result := True;
     end;
{$ENDIF}

end;

procedure TDebugWin.HandleForException(handleException: Integer);
(*******************************************************************************)
begin
	   Fm_continueWithException := handleException;
	   PulseEvent(Fm_waitForGUI);
end;

procedure TDebugWin.SetNewThreadContext(isWow64: Boolean; newProcessContext,newWowProcessContext: Winapi.Windows.PContext);
(*******************************************************************************)
begin
    if isWow64  then   FWowProcessContext := newWowProcessContext
    else               FpCtx := newProcessContext;
end;

function TDebugWin.GetContextDataEx(hActiveThread : THandle; IndexOfRegister: DWORD): LongInt;
(*******************************************************************************)
begin
	    ZeroMemory(FpCtx, SizeOf(CONTEXT));
	    FpCtx.ContextFlags := CONTEXT_FULL;
      Result := 0;
{$IF DEFINED(CPUX86)}
      GetThreadContext(hActiveThread, FpCtx^);
      if     (IndexOfRegister = R_EAX)   then Result := FpCtx.Eax
      else if(IndexOfRegister = R_EBX)   then Result := FpCtx.Ebx
      else if(IndexOfRegister = R_ECX)   then Result := FpCtx.Ecx
      else if(IndexOfRegister = R_EDX)   then Result := FpCtx.Edx
      else if(IndexOfRegister = R_EDI)   then Result := FpCtx.Edi
      else if(IndexOfRegister = R_ESI)   then Result := FpCtx.Esi
      else if(IndexOfRegister = R_EBP)   then Result := FpCtx.Ebp
      else if(IndexOfRegister = R_ESP)   then Result := FpCtx.Esp
      else if(IndexOfRegister = R_EIP)   then Result := FpCtx.Eip
      else if(IndexOfRegister = R_EFLAGS)then Result := FpCtx.EFlags
      else if(IndexOfRegister = R_DR0)   then Result := FpCtx.Dr0
      else if(IndexOfRegister = R_DR1)   then Result := FpCtx.Dr1
      else if(IndexOfRegister = R_DR2)   then Result := FpCtx.Dr2
      else if(IndexOfRegister = R_DR3)   then Result := FpCtx.Dr3
      else if(IndexOfRegister = R_DR6)   then Result := FpCtx.Dr6
      else if(IndexOfRegister = R_DR7)   then Result := FpCtx.Dr7
      else if(IndexOfRegister = R_CIP)   then Result := FpCtx.Eip
      else if(IndexOfRegister = R_CSP)   then Result := FpCtx.Esp
      else if(IndexOfRegister = R_SEG_GS)then Result := FpCtx.SegGs
      else if(IndexOfRegister = R_SEG_FS)then Result := FpCtx.SegFs
      else if(IndexOfRegister = R_SEG_ES)then Result := FpCtx.SegEs
      else if(IndexOfRegister = R_SEG_DS)then Result := FpCtx.SegDs
      else if(IndexOfRegister = R_SEG_CS)then Result := FpCtx.SegCs
      else if(IndexOfRegister = R_SEG_SS)then Result := FpCtx.SegSs
{$ELSEIF DEFINED(CPUX64)}
      GetThreadContext(hActiveThread, FpCtx^);
      if     (IndexOfRegister = R_EAX)   then Result := FpCtx.Rax
      else if(IndexOfRegister = R_EBX)   then Result := FpCtx.Rbx
      else if(IndexOfRegister = R_ECX)   then Result := FpCtx.Rcx
      else if(IndexOfRegister = R_EDX)   then Result := FpCtx.Rdx
      else if(IndexOfRegister = R_EDI)   then Result := FpCtx.Rdi
      else if(IndexOfRegister = R_ESI)   then Result := FpCtx.Rsi
      else if(IndexOfRegister = R_EBP)   then Result := FpCtx.Rbp
      else if(IndexOfRegister = R_ESP)   then Result := FpCtx.Rsp
      else if(IndexOfRegister = R_EIP)   then Result := FpCtx.Rip
      else if(IndexOfRegister = R_EFLAGS)then Result := FpCtx.EFlags
      else if(IndexOfRegister = R_RAX)   then Result := FpCtx.Rax
      else if(IndexOfRegister = R_EBX)   then Result := FpCtx.Rbx
      else if(IndexOfRegister = R_ECX)   then Result := FpCtx.Rcx
      else if(IndexOfRegister = R_RDX)   then Result := FpCtx.Rdx
      else if(IndexOfRegister = R_RDI)   then Result := FpCtx.Rdi
      else if(IndexOfRegister = R_RSI)   then Result := FpCtx.Rsi
      else if(IndexOfRegister = R_RBP)   then Result := FpCtx.Rbp
      else if(IndexOfRegister = R_RSP)   then Result := FpCtx.Rsp
      else if(IndexOfRegister = R_RIP)   then Result := FpCtx.Rip
      else if(IndexOfRegister = R_EFLAGS)then Result := FpCtx.EFlags
      else if(IndexOfRegister = R_DR0)   then Result := FpCtx.Dr0
      else if(IndexOfRegister = R_DR1)   then Result := FpCtx.Dr1
      else if(IndexOfRegister = R_DR2)   then Result := FpCtx.Dr2
      else if(IndexOfRegister = R_DR3)   then Result := FpCtx.Dr3
      else if(IndexOfRegister = R_DR6)   then Result := FpCtx.Dr6
      else if(IndexOfRegister = R_DR7)   then Result := FpCtx.Dr7
      else if(IndexOfRegister = R_R8)    then Result := FpCtx.R8
      else if(IndexOfRegister = R_R9)    then Result := FpCtx.R9
      else if(IndexOfRegister = R_R10)   then Result := FpCtx.R10
      else if(IndexOfRegister = R_R11)   then Result := FpCtx.R11
      else if(IndexOfRegister = R_R12)   then Result := FpCtx.R12
      else if(IndexOfRegister = R_R13)   then Result := FpCtx.R13
      else if(IndexOfRegister = R_R14)   then Result := FpCtx.R14
      else if(IndexOfRegister = R_R15)   then Result := FpCtx.R15
      else if(IndexOfRegister = R_CIP)   then Result := FpCtx.Rip
      else if(IndexOfRegister = R_CSP)   then Result := FpCtx.Rsp
      else if(IndexOfRegister = R_SEG_GS)then Result := FpCtx.SegGs
      else if(IndexOfRegister = R_SEG_FS)then Result := FpCtx.SegFs
      else if(IndexOfRegister = R_SEG_ES)then Result := FpCtx.SegEs
      else if(IndexOfRegister = R_SEG_DS)then Result := FpCtx.SegDs
      else if(IndexOfRegister = R_SEG_CS)then Result := FpCtx.SegCs
      else if(IndexOfRegister = R_SEG_SS)then Result := FpCtx.SegSs
{$ENDIF}
end;

function TDebugWin.SetContextDataEx(hActiveThread : THandle; IndexOfRegister: DWORD; NewRegValue:LongInt): Boolean;
(*******************************************************************************)
begin
	    ZeroMemory(FpCtx, SizeOf(CONTEXT));
	    FpCtx.ContextFlags := CONTEXT_FULL;
      Result := True;

{$IF DEFINED(CPUX86)}
      GetThreadContext(hActiveThread, FpCtx^);
      if     (IndexOfRegister = R_EAX)   then FpCtx.Eax   := NewRegValue
      else if(IndexOfRegister = R_EBX)   then FpCtx.Ebx   := NewRegValue
      else if(IndexOfRegister = R_ECX)   then FpCtx.Ecx   := NewRegValue
      else if(IndexOfRegister = R_EDX)   then FpCtx.Edx   := NewRegValue
      else if(IndexOfRegister = R_EDI)   then FpCtx.Edi   := NewRegValue
      else if(IndexOfRegister = R_ESI)   then FpCtx.Esi   := NewRegValue
      else if(IndexOfRegister = R_EBP)   then FpCtx.Ebp   := NewRegValue
      else if(IndexOfRegister = R_ESP)   then FpCtx.Esp   := NewRegValue
      else if(IndexOfRegister = R_EIP)   then FpCtx.Eip   := NewRegValue
      else if(IndexOfRegister = R_EFLAGS)then FpCtx.EFlags:= NewRegValue
      else if(IndexOfRegister = R_DR0)   then FpCtx.Dr0   := NewRegValue
      else if(IndexOfRegister = R_DR1)   then FpCtx.Dr1   := NewRegValue
      else if(IndexOfRegister = R_DR2)   then FpCtx.Dr2   := NewRegValue
      else if(IndexOfRegister = R_DR3)   then FpCtx.Dr3   := NewRegValue
      else if(IndexOfRegister = R_DR6)   then FpCtx.Dr6   := NewRegValue
      else if(IndexOfRegister = R_DR7)   then FpCtx.Dr7   := NewRegValue
      else if(IndexOfRegister = R_CIP)   then FpCtx.Eip   := NewRegValue
      else if(IndexOfRegister = R_CSP)   then FpCtx.Esp   := NewRegValue
      else if(IndexOfRegister = R_SEG_GS)then FpCtx.SegGs := NewRegValue
      else if(IndexOfRegister = R_SEG_FS)then FpCtx.SegFs := NewRegValue
      else if(IndexOfRegister = R_SEG_ES)then FpCtx.SegEs := NewRegValue
      else if(IndexOfRegister = R_SEG_DS)then FpCtx.SegDs := NewRegValue
      else if(IndexOfRegister = R_SEG_CS)then FpCtx.SegCs := NewRegValue
      else if(IndexOfRegister = R_SEG_SS)then FpCtx.SegSs := NewRegValue
      else Result := False;

      if Result then
         if not (SetThreadContext(hActiveThread, FpCtx^)) then Result := False;
{$ELSEIF DEFINED(CPUX64)}
      GetThreadContext(hActiveThread, FpCtx^);
      if (IndexOfRegister = R_EAX) then
      begin
          NewRegValue := FpCtx.Rax - DWORD(FpCtx.Rax) + NewRegValue;
          FpCtx.Rax   := NewRegValue;
      end
      else if (IndexOfRegister = R_EBX) then
      begin
          NewRegValue := FpCtx.Rbx - DWORD(FpCtx.Rbx) + NewRegValue;
          FpCtx.Rbx   := NewRegValue;
      end
      else if (IndexOfRegister = R_ECX) then
      begin
          NewRegValue := FpCtx.Rcx - DWORD(FpCtx.Rcx) + NewRegValue;
          FpCtx.Rcx   := NewRegValue;
      end
      else if (IndexOfRegister = R_EDX) then
      begin
          NewRegValue := FpCtx.Rdx - DWORD(FpCtx.Rdx) + NewRegValue;
          FpCtx.Rdx   := NewRegValue;
      end
      else if (IndexOfRegister = R_EDI) then
      begin
          NewRegValue := FpCtx.Rdi - DWORD(FpCtx.Rdi) + NewRegValue;
          FpCtx.Rdi   := NewRegValue;
      end
      else if (IndexOfRegister = R_ESI) then
      begin
          NewRegValue := FpCtx.Rsi - DWORD(FpCtx.Rsi) + NewRegValue;
          FpCtx.Rsi   := NewRegValue;
      end
      else if (IndexOfRegister = R_EBP) then
      begin
          NewRegValue := FpCtx.Rbp - DWORD(FpCtx.Rbp) + NewRegValue;
          FpCtx.Rbp   := NewRegValue;
      end
      else if (IndexOfRegister = R_ESP) then
      begin
          NewRegValue := FpCtx.Rsp - DWORD(FpCtx.Rsp) + NewRegValue;
          FpCtx.Rsp   := NewRegValue;
      end
      else if(IndexOfRegister = R_EIP) then
      begin
          NewRegValue := FpCtx.Rip - DWORD(FpCtx.Rip) + NewRegValue;
          FpCtx.Rip   := NewRegValue;
      end
      else if (IndexOfRegister = R_EFLAGS) then FpCtx.EFlags := DWORD(NewRegValue)
      else if (IndexOfRegister = R_RAX)    then FpCtx.Rax := NewRegValue
      else if (IndexOfRegister = R_RBX)    then FpCtx.Rbx := NewRegValue
      else if (IndexOfRegister = R_RCX)    then FpCtx.Rcx := NewRegValue
      else if (IndexOfRegister = R_RDX)    then FpCtx.Rdx := NewRegValue
      else if (IndexOfRegister = R_RDI)    then FpCtx.Rdi := NewRegValue
      else if (IndexOfRegister = R_RSI)    then FpCtx.Rsi := NewRegValue
      else if (IndexOfRegister = R_RBP)    then FpCtx.Rbp := NewRegValue
      else if (IndexOfRegister = R_RSP)    then FpCtx.Rsp := NewRegValue
      else if (IndexOfRegister = R_RIP)    then FpCtx.Rip := NewRegValue
      else if (IndexOfRegister = R_RFLAGS) then FpCtx.EFlags := DWORD(NewRegValue)
      else if (IndexOfRegister = R_DR0)    then FpCtx.Dr0 := NewRegValue
      else if (IndexOfRegister = R_DR1)    then FpCtx.Dr1 := NewRegValue
      else if (IndexOfRegister = R_DR2)    then FpCtx.Dr2 := NewRegValue
      else if (IndexOfRegister = R_DR3)    then FpCtx.Dr3 := NewRegValue
      else if (IndexOfRegister = R_DR6)    then FpCtx.Dr6 := NewRegValue
      else if (IndexOfRegister = R_DR7)    then FpCtx.Dr7 := NewRegValue
      else if (IndexOfRegister = R_R8)     then FpCtx.R8  := NewRegValue
      else if (IndexOfRegister = R_R9)     then FpCtx.R9  := NewRegValue
      else if (IndexOfRegister = R_R10)    then FpCtx.R10 := NewRegValue
      else if (IndexOfRegister = R_R11)    then FpCtx.R11 := NewRegValue
      else if (IndexOfRegister = R_R12)    then FpCtx.R12 := NewRegValue
      else if (IndexOfRegister = R_R13)    then FpCtx.R13 := NewRegValue
      else if (IndexOfRegister = R_R14)    then FpCtx.R14 := NewRegValue
      else if (IndexOfRegister = R_R15)    then FpCtx.R15 := NewRegValue
      else if (IndexOfRegister = R_CIP)    then FpCtx.Rip := NewRegValue
      else if (IndexOfRegister = R_CSP)    then FpCtx.Rsp := NewRegValue
      else if (IndexOfRegister = R_SEG_GS) then FpCtx.SegGs := WORD(NewRegValue)
      else if (IndexOfRegister = R_SEG_FS) then FpCtx.SegFs := WORD(NewRegValue)
      else if (IndexOfRegister = R_SEG_ES) then FpCtx.SegEs := WORD(NewRegValue)
      else if (IndexOfRegister = R_SEG_DS) then FpCtx.SegDs := WORD(NewRegValue)
      else if (IndexOfRegister = R_SEG_CS) then FpCtx.SegCs := WORD(NewRegValue)
      else if (IndexOfRegister = R_SEG_SS) then FpCtx.SegSs := WORD(NewRegValue)
      else Result := False;

      if Result then
         if not (SetThreadContext(hActiveThread, FpCtx^)) then Result := False;
{$ENDIF}
end;

function TDebugWin.GetContextData(IndexOfRegister: DWORD): LongInt;
(*******************************************************************************)
var
  hActiveThread : THandle;
begin
     hActiveThread := OpenThread(THREAD_GETSET_CONTEXT, false, FDebugEvent.dwThreadId);
     Result        := GetContextDataEx(hActiveThread, IndexOfRegister);
	   CloseHandle(hActiveThread);
end;

function TDebugWin.SetContextData(IndexOfRegister: DWORD; NewRegValue:LongInt):Boolean;
(*******************************************************************************)
var
  hActiveThread : THandle;

begin
     hActiveThread := OpenThread(THREAD_GETSET_CONTEXT, false, FDebugEvent.dwThreadId);
	   Result        := SetContextDataEx(hActiveThread, IndexOfRegister,NewRegValue);
	   CloseHandle(hActiveThread);
end;

function TDebugWin.ReadMemoryFromDebugee(dwPID: DWORD; dwAddress: nativeUInt;dwSize: DWORD; lpBuffer: Pointer): Boolean;
(*******************************************************************************)
var
  i : Integer;
  nb: NativeUInt;
begin
      for i := 0 to FPIDs.Count -1 do
      begin
           if(dwPID = PPIDStruct(FPIDs[i])^.dwPID)  then
           begin
              Result := ReadProcessMemory(PPIDStruct(FPIDs[i])^.hProc,Pointer(dwAddress),lpBuffer,dwSize,nb);
              Exit;
           end;
      end;
      Result := False;
end;

function TDebugWin.WriteMemoryFromDebugee(dwPID: DWORD;dwAddress: NativeUInt;dwSize: DWORD; lpBuffer:Pointer): Boolean;
(*******************************************************************************)
var
  i : Integer;
  nb: NativeUInt;
begin
     for i := 0 to FPIDs.Count -1 do
     begin
           if(dwPID = PPIDStruct(FPIDs[i])^.dwPID)  then
           begin
                Result := WriteProcessMemory(PPIDStruct(FPIDs[i])^.hProc,Pointer(dwAddress),lpBuffer,dwSize,nB);
                Exit;
           end;
     end;
     Result := False;
end;

function TDebugWin.GetDebugData :Pointer;
(*******************************************************************************)
begin
	   Result := @FDebugEvent;
end;

function TDebugWin.GetProcessInformation:Pointer;
(*******************************************************************************)
begin
	   Result := @FProcessInfo;
end;

(*************Gestione BreakPoint**********************)
(******************************************************)

function TDebugWin.SetBPX(dwPID: DWORD; bpxAddress: NativeUInt;dwKeep,dwSize: DWORD; var bOrgByte : byte): Boolean;
(*******************************************************************************)
var
  i            : Integer;
  bOld         : byte;
  nB           : NativeUInt;
  hPID         : THandle;
begin
     Result := False;
     if bpxAddress = 0 then Exit;

     hPID   := FProcessInfo.hProcess;

     if(dwPID <> Cardinal(-1)) then
	   begin
		      for i := 0 to FPIDs.Count -1 do
		      begin
			         if(dwPID = PPIDStruct(FPIDs[i])^.dwPID)  then
				           hPID := PPIDStruct(FPIDs[i])^.hProc;
		      end;
   	 end;

     if(ReadProcessMemory(hPID, Pointer(bpxAddress),@bOld, dwSize, nB)) then
     begin
          if(bOld <> $CC)  then
          begin
               bOrgByte := bOld;
               if(WriteProcessMemory(hPID, Pointer(bpxAddress), @INT3BreakPoint, dwSize, nB)) then
                  Result := True;
          end
     end;

end;

function TDebugWin.dBPX(const dwPID: DWORD; dwOffset: NativeUint;dwSize : DWORD; btOrgByte: Byte) : Boolean;
(*******************************************************************************)
var
  dwBytesWritten,
  hPID           : THandle;
  i              : Integer;
begin
     Result := False;
	   if(dwOffset = 0) and (btOrgByte = 0) then Exit;

     hPID   := FProcessInfo.hProcess;

     if(dwPID <> Cardinal(-1)) then
	   begin
		      for i := 0 to FPIDs.Count -1 do
		      begin
			         if(dwPID = PPIDStruct(FPIDs[i])^.dwPID)  then
				           hPID := PPIDStruct(FPIDs[i])^.hProc;
		      end;
   	 end;

	   if WriteProcessMemory(hPID,Pointer(dwOffset),@btOrgByte,dwSize, NativeUInt(dwBytesWritten)) then Result := True;
end;

function TDebugWin.SetMemoryBP(dwPID: DWORD; MemoryStart : NativeUInt;SizeOfMemory,dwKeep: DWORD ):Boolean;
(*******************************************************************************)
var
   i          : Integer;
	 memInfo    : WinApi.Windows.MEMORY_BASIC_INFORMATION;
   hPID       : THandle;
   OldProtect : DWORD;

begin
     Result := False;
     hPID   := FProcessInfo.hProcess;
     for i := 0 to FPIDs.Count -1 do
     begin
          if(dwPID = PPIDStruct(FPIDs[i])^.dwPID)  then
               hPID := PPIDStruct(FPIDs[i])^.hProc;
     end;

     VirtualQueryEx(hPID, Pointer(MemoryStart), MemInfo, SizeOf(MEMORY_BASIC_INFORMATION));
     if VirtualProtectEx(hPID, Pointer(MemoryStart), SizeOfMemory, MemInfo.Protect or PAGE_GUARD, OldProtect) then Result := True;
end;

function TDebugWin.dMemoryBP(dwPId : DWORD; MemoryStart, SizeOfMemory: LongInt): Boolean;
(*******************************************************************************)
var
   i          : Integer;
	 memInfo    : WinApi.Windows.MEMORY_BASIC_INFORMATION;
   hPID       : THandle;
   OldProtect : DWORD;
begin
     Result := False;
     hPID   := FProcessInfo.hProcess;
     for i := 0 to FPIDs.Count -1 do
     begin
          if(dwPID = PPIDStruct(FPIDs[i])^.dwPID)  then
               hPID := PPIDStruct(FPIDs[i])^.hProc;
     end;

     VirtualQueryEx(hPID, Pointer(MemoryStart), MemInfo, SizeOf(MEMORY_BASIC_INFORMATION));
     if VirtualProtectEx(hPID, Pointer(MemoryStart), SizeOfMemory, memInfo.Protect and PAGE_GUARD, OldProtect) then Result := True;
end;

function TDebugWin.SetHardwareBreakPoint(dwPID : DWORD; bpxAddress: NativeUInt;bpxSize,bpxSlot, bpxFlag : DWORD):Boolean;
(*******************************************************************************)
var
   te32       : TThreadEntry32;
   cTT        : WinApi.Windows.PContext;
   hThread    : THandle;
   dwThrCount : DWORD;
   hThreadSnap: Cardinal;
   i,iBp      : Integer;
   DRMask,
   ClearMask  : DWORD;

begin
     Result := False;
     DRMask := 0;

     if dwPID = Cardinal(-1) then dwPID := FProcessInfo.dwProcessId;

     if not (( bpxSize = 1)  or (bpxSize = 2) or (bpxSize = 4)) then  Exit;

	   if not ((bpxFlag = DR_EXECUTE) or (bpxFlag = DR_READ) or (bpxFlag = DR_WRITE)) then Exit;

     iBp := 0;
     for i := 0 to FBPHs.Count -1 do
     begin
          if(bpxAddress = PBreakPoint(FBPHs[i])^.bpxAddress)  then
               iBp := i;
     end;
     // Calcolo maschera bit x dr7
     OutputDebugString(PChar('1:Debugregistermask=' + IntToHex(DRMask, 8)));

     case bpxFlag of
       DR_WRITE: DRMask := $1 or DRMask;
       DR_READ : DRMask := $3 or DRMask;
     end;


     case bpxSize of
       2: DRMask := $4 or DRMask;
       4: DRMask := $c or DRMask;
       8: DRMask := $8 or DRMask; //10 is defined as 8 byte
     end;
     OutputDebugString(PChar('2:Debugregistermask=' + IntToHex(DRMask, 8)));

     DRMask := (DRMask shl (16 + 4 * PBreakPoint(FBPHs[iBP])^.bpxSlot));
     //set the RWx amd LENx to the proper position
     DRMask := DRMask or (3 shl (PBreakPoint(FBPHs[iBP])^.bpxSlot * 2));
     //and set the Lx bit
     DRMask := DRMask or (1 shl 10); //and set bit 10 to 1

     ClearMask := (($F shl (16 + 4 * PBreakPoint(FBPHs[iBP])^.bpxSlot)) or (3 shl (PBreakPoint(FBPHs[iBP])^.bpxSlot * 2))) xor $FFFFFFFF;
     //create a mask that can be used to undo the old settings

     OutputDebugString(PChar('3:Debugregistermask=' + IntToHex(DRMask, 8)));
     OutputDebugString(PChar('clearmask=' + IntToHex(ClearMask, 8)));
     // fine calcolo maschera bit x dr7

     hThreadSnap := CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, dwPID);
     If (hThreadSnap = INVALID_HANDLE_VALUE) Then  Exit;

     te32.dwSize := SizeOf(THREADENTRY32);
     If not (Thread32First(hThreadSnap, te32)) Then
     begin
          CloseHandle(hThreadSnap);
          Exit;
     end;

     try
         dwThrCount      := 0;
         hThread         := INVALID_HANDLE_VALUE;
         GetMem(cTT,Sizeof(TCONTEXT));
         cTT^.ContextFlags := CONTEXT_ALL;

         Repeat
            If (te32.th32OwnerProcessID = dwPID) Then hThread := OpenThread(THREAD_GETSET_CONTEXT,False,te32.th32ThreadID);
            if hThread <> INVALID_HANDLE_VALUE then
            begin
                 try
                     inc(dwThrCount);
                     if not GetThreadContext(hThread,cTT^) then Exit;
                     case PBreakPoint(FBPHs[iBP])^.bpxSlot of
                        0: cTT^.Dr0 := PBreakPoint(FBPHs[iBP])^.bpxAddress;
                        1: cTT^.Dr1 := PBreakPoint(FBPHs[iBP])^.bpxAddress;
                        2: cTT^.Dr2 := PBreakPoint(FBPHs[iBP])^.bpxAddress;
                        3: cTT^.Dr3 := PBreakPoint(FBPHs[iBP])^.bpxAddress;
                     end;

                     cTT^.Dr7 :=(cTT^.Dr7 and ClearMask) or DRMask;
                     SetThreadContext(hThread,cTT^);

                 finally
                     CloseHandle(hThread);
                 end;
            end;

         Until (Not Thread32Next(hThreadSnap, te32));
         FMsg := Format(rsInsertNewHBP,[dwThrCount,PBreakPoint(FBPHs[iBP])^.bpxAddress,PBreakPoint(FBPHs[iBP])^.bpxSlot]);
         Synchronize(PBLogInfo);
         Result := True;
     finally
         CloseHandle(hThreadSnap);
     end;

end;

function TDebugWin.dHardwareBP(dwPID : DWORD; bpxAddress: NativeUInt;bpxSlot: DWORD):Boolean;
(*******************************************************************************)
var
   te32       : TThreadEntry32;
   cTT        : WinApi.Windows.PContext;
   hThread    : THandle;
   hThreadSnap: Cardinal;
   DRMask     : DWORD;
begin
     Result := False;

     hThreadSnap := CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, dwPID);
     If (hThreadSnap = INVALID_HANDLE_VALUE) Then  Exit;

     te32.dwSize := SizeOf(THREADENTRY32);
     If not (Thread32First(hThreadSnap, te32)) Then
     begin
          CloseHandle(hThreadSnap);
          Exit;
     end;

     DRMask := $F shl (16 + 4 * bpxSlot) + (3 shl (bpxSlot * 2));
     DRMask := not DRMask;

     try
         hThread := INVALID_HANDLE_VALUE;
         GetMem(cTT,Sizeof(TCONTEXT));
         cTT^.ContextFlags := CONTEXT_FULL;
         Repeat
            If (te32.th32OwnerProcessID = dwPID) Then   hThread := OpenThread(THREAD_GETSET_CONTEXT,False,te32.th32ThreadID);

            if hThread <> INVALID_HANDLE_VALUE then
            begin
                try
                     GetThreadContext(hThread,cTT^);
                     if      cTT^.Dr0 = Cardinal(bpxAddress)  then cTT^.Dr0 := 0
                     else if cTT^.Dr1 = Cardinal(bpxAddress)  then cTT^.Dr1 := 0
                     else if cTT^.Dr2 = Cardinal(bpxAddress)  then cTT^.Dr2 := 0
                     else if cTT^.Dr3 = Cardinal(bpxAddress)  then cTT^.Dr3 := 0 ;

                     cTT^.Dr7 := cTT^.Dr7 and  DRMask;
                     SetThreadContext(hThread,cTT^);
                finally
                    CloseHandle(hThread);
                end;
            end;
         Until (Not Thread32Next(hThreadSnap, te32));
         Result := True;
     finally
         CloseHandle(hThreadSnap);
     end;

end;

(* Set BPs
	|
	| dwType:
	|		0 = SoftwareBP
	|	  1 = MemoryBP
	|		2 = HardwareBP
	|
	| dwTypeFlag: Only for HardwareBP currently
	|		DR_EXECUTE	(0x00) - Break on execute
	|		DR_WRITE  	(0x01) - Break on data writes
	|		DR_READ	  	(0x11) - Break on data reads or writes.
	|
	| dwPID: ProcessID where you want to place the BP ( must be a process that is debugged )
	|		 Can be -1 for mother process if you have different child processes
	|
	| dwOffset: Address where the BP will be placed
	|
	| dwSlot: Only for HW Breakpoints
	|		0 - Dr0
	|		1 - Dr1
	|		2 - Dr2
	|		3 - Dr3
	|
	| dwKeep: set true if the BP should be placed again after one hit
	|
	| Notes: - For HardwareBPs you will need admin rights, else they will not work...
	|
	*)
Procedure TDebugWin.AddBreakpointToList(dwBPType,dwTypeFlag,dwPID: DWORD; dwOffset: NativeUInt; dwSlot,dwKeep: DWORD;pBPCallBack: Pointer);
(*******************************************************************************)
var
   bExists : Boolean;
   i       : Integer;
   newBP   : PBreakPoint;
   dwSize  : Cardinal;
   bSlot1,
   bSlot2,
   bSlot3,
   bSlot4  : Boolean;
begin
	   bExists := False;

     for i := 0 to FBPXs.Count - 1 do
     begin
          newBP:=PBreakPoint(FBPXs.Items[i]);
          if (newBP^.bpxAddress = dwOffset) then
          begin
               bExists := True;
               Break;
          end;
     end;
     for i := 0 to FBPMs.Count - 1 do
     begin
          newBP:=PBreakPoint(FBPMs.Items[i]);
          if (newBP^.bpxAddress = dwOffset) then
          begin
               bExists := True;
               Break;
          end;
     end;
     for i := 0 to FBPHs.Count - 1 do
     begin
          newBP:=PBreakPoint(FBPHs.Items[i]);
          if (newBP^.bpxAddress = dwOffset) then
          begin
               bExists := True;
               Break;
          end;
     end;

     if  not bExists then
     begin
          dwSize := 1;
          case dwBPType of
           0: begin        //SoftwareBP
                   GetMem(newBP,SizeOf(BreakPoint));
                   ZeroMemory(newBP,SizeOf(BreakPoint));

                   newBP^.bpxAddress  := dwOffset;
                   newBP^.dwHandle    := dwKeep;
                   newBP^.bpxSize     := dwSize;
                   SetBPX(dwPID,dwOffset,dwKeep,dwSize,newBP^.OriginalByte);
                   newBP^.dwPID       := dwPID;
                   newBP^.bRestoreBP  := False;
                   newBP^.bpxType     := dwTypeFlag;
                   newBP^.bpxCallBack := pBPCallBack;
                   newBP^.moduleName  := StrAlloc(MAX_PATH);
                   ZeroMemory(newBP.moduleName,MAX_PATH * SizeOf(Char));
                   if dwKeep = $1 then
					           newBP^.bpxBaseOffset := CalcOffsetForModule(newBP.moduleName,newBP.bpxAddress,newBP.dwPID);

                   FBPXs.Add(newBP);
                   if Assigned(FOnNewBPAdded) then  FOnNewBPAdded(newBP^,0);
              end;
           1: begin    		//MemoryBP
                   GetMem(newBP,SizeOf(BreakPoint));
                   ZeroMemory(newBP,SizeOf(BreakPoint));

                   newBP^.bpxAddress  := dwOffset;
                   newBP^.dwHandle    := dwKeep;
                   newBP^.bpxSize     := dwSize;
                   newBP^.OriginalByte:= 0;
                   newBP^.dwPID       := dwPID;
                   newBP^.bRestoreBP  := False;
                   newBP^.bpxType     := dwTypeFlag;
                   SetMemoryBP(dwPID,dwOffset,dwSize,dwKeep) ;
                   newBP^.bpxCallBack := pBPCallBack;
                   newBP^.moduleName  := StrAlloc(MAX_PATH);
                   ZeroMemory(newBP.moduleName,MAX_PATH * SizeOf(Char));
                   newBP^.bpxBaseOffset := CalcOffsetForModule(newBP.moduleName,newBP.bpxAddress,newBP.dwPID);

                   FBPMs.Add(newBP);
                   if Assigned(FOnNewBPAdded) then  FOnNewBPAdded(newBP^,1);
              end;
           2: begin  				//HardwareBP
                   if FBPHs.Count = 4 then Exit;

                   GetMem(newBP,SizeOf(BreakPoint));
                   ZeroMemory(newBP,SizeOf(BreakPoint));

                   newBP^.bpxAddress  := dwOffset;
                   newBP^.dwHandle    := dwKeep;
                   newBP^.bpxSize     := 1;
                   newBP^.OriginalByte:= 0;
                   newBP^.dwPID       := dwPID;
                   newBP^.bRestoreBP  := False;
                   newBP^.bpxType     := dwTypeFlag;
                   newBP^.bpxCallBack := pBPCallBack;
                   newBP^.moduleName  := StrAlloc(MAX_PATH);
                   ZeroMemory(newBP.moduleName,MAX_PATH * SizeOf(Char));
           		     newBP^.bpxBaseOffset := CalcOffsetForModule(newBP.moduleName,newBP.bpxAddress,newBP.dwPID);

                   bSlot1 := False; bSlot2 := False;bSlot3 := False;bSlot4 := False;
                   for i := 0 to FBPHs.Count - 1 do
                   begin
                        case PBreakPoint(FBPHs.Items[i])^.bpxSlot of
                           0: bSlot1 := True;
                           1: bSlot2 := True;
                           2: bSlot3 := True;
                           3: bSlot4 := True;
                        end;

                   end;

                  if       not bSlot1 then newBP^.bpxSlot := 0
                  else if  not bSlot2 then newBP^.bpxSlot := 1
                  else if  not bSlot3 then newBP^.bpxSlot := 2
                  else if  not bSlot4 then newBP^.bpxSlot := 3;

                  FBPHs.Add(newBP);
                  SetHardwareBreakPoint(dwPID,dwOffset,dwSize,newBP.bpxSlot,dwTypeFlag);
                  if Assigned(FOnNewBPAdded) then  FOnNewBPAdded(newBP^,2);
              end
          end;
     end; // if

end;

function TDebugWin.RemoveBPFromList(dwOffset: NativeUInt; dwType: DWORD): Boolean;
(*******************************************************************************)
var
  i   : integer;
  bpp : PBreakPoint;
begin

     case dwType of
         BP_SOFTBPX : begin
                            for i := 0 to FBPXs.Count - 1 do
                            begin
                                 bpp:=PBreakPoint(FBPXs.Items[i]);
                                 if (bpp.bpxAddress = dwOffset) then
                                 begin
                                      dBPX(bpp.dwPID,bpp.bpxAddress,bpp.bpxSize,bpp.OriginalByte);
                                      FBPXs.Delete(i);
                                      FreeMem(bpp);
                                      Break;
                                 end;
                            end;
                      end;
         BP_MEMORYBPX:begin
                           for i := 0 to FBPMs.Count - 1 do
                           begin
                                 bpp:=PBreakPoint(FBPMs.Items[i]);
                                 if (bpp.bpxAddress = dwOffset) then
                                 begin
                                      dMemoryBP(bpp.dwPID,bpp.bpxAddress,bpp.bpxSize);
                                      FBPMs.Delete(i);
                                      FreeMem(bpp);
                                      Break;
                                 end;
                           end;
                      end;
       BP_HARDWAREBPX:begin
                           for i := 0 to FBPHs.Count - 1 do
                           begin
                                 bpp:=PBreakPoint(FBPHs.Items[i]);
                                 if (bpp.bpxAddress = dwOffset)  then
                                 begin
                                      dHardwareBP(bpp.dwPID,bpp.bpxAddress,bpp.bpxSlot);
                                      FBPMs.Delete(i);
                                      FreeMem(bpp);
                                      Break;
                                 end;
                           end;
                      end;
     end;
     if Assigned(FOnBPDeleted) then  FOnBPDeleted(dwOffset);

     Result := True;

end;

function TDebugWin.RemoveBPs: Boolean;
(*******************************************************************************)
var
  i   : Integer;
  bpp : PBreakPoint;
begin

     for i := 0 to FBPXs.Count - 1 do
     begin
          bpp:=PBreakPoint(FBPXs.Items[i]);
          RemoveBPFromList(bpp^.bpxAddress,0);
     end;

     for i := 0 to FBPMs.Count - 1 do
     begin
          bpp:=PBreakPoint(FBPMs.Items[i]);
          RemoveBPFromList(bpp^.bpxAddress,1);
     end;

     for i := 0 to FBPHs.Count - 1 do
     begin
          bpp:=PBreakPoint(FBPHs.Items[i]);
          RemoveBPFromList(bpp^.bpxAddress,2);
     end;

     FBPXs.clear();
     FBPMs.clear();
     FBPHs.clear();

     Result := True;

end;

function TDebugWin.InitBP: Boolean;
(*******************************************************************************)
var
  i : Integer;
begin

     for i := 0 to FBPXs.Count - 1 do
        SetBPX(PBreakPoint(FBPXs[i])^.dwPID,PBreakPoint(FBPXs[i])^.bpxAddress,PBreakPoint(FBPXs[i])^.dwHandle,PBreakPoint(FBPXs[i])^.bpxSize,PBreakPoint(FBPXs[i])^.OriginalByte);

     for i := 0 to FBPMs.Count - 1 do
        SetMemoryBP(PBreakPoint(FBPMs[i])^.dwPID,PBreakPoint(FBPMs[i])^.bpxAddress,PBreakPoint(FBPMs[i])^.bpxSize,PBreakPoint(FBPMs[i])^.dwHandle);

     for i := 0 to FBPHs.Count - 1 do
        SetHardwareBreakPoint(PBreakPoint(FBPHs[i])^.dwPID,PBreakPoint(FBPHs[i])^.bpxAddress,PBreakPoint(FBPHs[i])^.bpxSize,PBreakPoint(FBPHs[i])^.bpxSlot,PBreakPoint(FBPHs[i])^.bpxType);

     Result := True;
end;

Procedure TDebugWin.UpdateOffsetsBPs;
(*******************************************************************************)
var
   i             : Integer;
   newBaseOffset,
   newOffset     : DWORD64;
   newBP         : PBreakPoint;
begin

     for i := 0 to FBPXs.Count - 1 do
     begin
          newBP := PBreakPoint(FBPXs.Items[i]);
          if newBP^.dwHandle <> $1 then Continue;

          newBaseOffset := CalcOffsetForModule(newBP^.moduleName,newBP^.bpxAddress,newBP^.dwPID);
          if (newBaseOffset <> newBP^.bpxAddress) and (newBaseOffset <> newBP^.bpxBaseOffset) then
          begin
               if newBP^.bpxBaseOffset = 0 then
               begin
                    newBP^.bpxOldOffset  := newBP^.bpxAddress;
                    newBP^.bpxBaseOffset := newBaseOffset;
               end else
               begin
                    newOffset := newBP^.bpxAddress - newBP^.bpxBaseOffset;

                    newBP^.bpxOldOffset  := newBP^.bpxAddress;
                    newBP^.bpxBaseOffset := newBaseOffset;
                    newBP^.bpxAddress    := newOffset + newBaseOffset;
               end;

               newBP^.dwHandle := $3;
               if Assigned(FOnNewBPAdded) then  FOnNewBPAdded(newBP^,0);
			         newBP^.dwHandle := $1;
          end;
     end;

     for i := 0 to FBPMs.Count - 1 do
     begin
          newBP := PBreakPoint(FBPMs.Items[i]);
          if newBP^.dwHandle <> $1 then Continue;

          newBaseOffset := CalcOffsetForModule(newBP^.moduleName,newBP^.bpxAddress,newBP^.dwPID);
          if (newBaseOffset <> newBP^.bpxAddress) and (newBaseOffset <> newBP^.bpxBaseOffset) then
          begin
               if newBP^.bpxBaseOffset = 0 then
               begin
                    newBP^.bpxOldOffset  := newBP^.bpxAddress;
                    newBP^.bpxBaseOffset := newBaseOffset;
               end else
               begin
                    newOffset := newBP^.bpxAddress - newBP^.bpxBaseOffset;

                    newBP^.bpxOldOffset  := newBP^.bpxAddress;
                    newBP^.bpxBaseOffset := newBaseOffset;
                    newBP^.bpxAddress     := newOffset + newBaseOffset;
               end;

               newBP^.dwHandle := $3;
			         if Assigned(FOnNewBPAdded) then  FOnNewBPAdded(newBP^,1);
			         newBP^.dwHandle := $1;
          end;
     end;

     for i := 0 to FBPHs.Count - 1 do
     begin
          newBP := PBreakPoint(FBPHs.Items[i]);
          if newBP^.dwHandle <> $1 then Continue;

          newBaseOffset := CalcOffsetForModule(newBP^.moduleName,newBP^.bpxAddress,newBP^.dwPID);
          if (newBaseOffset <> newBP^.bpxAddress) and (newBaseOffset <> newBP^.bpxBaseOffset) then
          begin
               if newBP^.bpxBaseOffset = 0 then
               begin
                    newBP^.bpxOldOffset  := newBP^.bpxAddress;
                    newBP^.bpxBaseOffset := newBaseOffset;
               end else
               begin
                    newOffset := newBP^.bpxAddress - newBP^.bpxBaseOffset;

                    newBP^.bpxOldOffset  := newBP^.bpxAddress;
                    newBP^.bpxBaseOffset := newBaseOffset;
                    newBP^.bpxAddress    := newOffset + newBaseOffset;
               end;

               newBP^.dwHandle := $3;
			         if Assigned(FOnNewBPAdded) then  FOnNewBPAdded(newBP^,1);
			         newBP^.dwHandle := $1;
          end;
     end;

end;

end.
