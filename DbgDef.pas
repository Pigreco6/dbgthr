unit DbgDef;

interface
     uses Winapi.Windows,jwaWinNt;

type NTSTATUS = LONG;

const

      BREAK_ON_EP        = 0;  // 0 = default application EP
      BREAK_ON_SYSTEM_EP = 1;  // 1 = SystemEP
      BREAK_ON_TLS       = 2;  // 2 = TLSCallback // Not working right now
      BREAK_ON_D_RUN     = 3;  // 3 = Direct run

      HANDLE_BREAK       = 0;  // 0 = Break and wait for User *DEFAULT*
      HANDLE_PASS        = 1;  // 1 = pass to App/Os
      HANDLE_IGNORE      = 2;  // 2 = Ignore
      HANDLE_J_STACK     = 3;  // 3 = automatic workaround ( jumping to the return addr which is on the stack )

      MAXIMUM_BREAKPOINTS = 1000;

      INT3BreakPoint : Byte = $CC;

      BP_SOFTBPX              = 0;
      BP_MEMORYBPX            = 1;
      BP_HARDWAREBPX          = 2;

      DR_EXECUTE              = 0;
      DR_WRITE                = 1;
      DR_READ                 = $11;


      R_EAX    = 1;
      R_EBX    = 2;
      R_ECX    = 3;
      R_EDX    = 4;
      R_EDI    = 5;
      R_ESI    = 6;
      R_EBP    = 7;
      R_ESP    = 8;
      R_EIP    = 9;
      R_EFLAGS = 10;
      R_DR0    = 11;
      R_DR1    = 12;
      R_DR2    = 13;
      R_DR3    = 14;
      R_DR6    = 15;
      R_DR7    = 16;
      R_RAX    = 17;
      R_RBX    = 18;
      R_RCX    = 19;
      R_RDX    = 20;
      R_RDI    = 21;
      R_RSI    = 22;
      R_RBP    = 23;
      R_RSP    = 24;
      R_RIP    = 25;
      R_RFLAGS = 26;
      R_R8     = 27;
      R_R9     = 28;
      R_R10    = 29;
      R_R11    = 30;
      R_R12    = 31;
      R_R13    = 32;
      R_R14    = 33;
      R_R15    = 34;
      R_CIP    = 35;
      R_CSP    = 36;
      R_SEG_GS = 37;
      R_SEG_FS = 38;
      R_SEG_ES = 39;
      R_SEG_DS = 40;
      R_SEG_CS = 41;
      R_SEG_SS = 42;

      THREAD_GETSET_CONTEXT       = (THREAD_SET_CONTEXT or THREAD_GET_CONTEXT); //or THREAD_QUERY_INFORMATION);



CONST
  STATUS_SUCCESS               = NTStatus($00000000);
  STATUS_ACCESS_DENIED         = NTStatus($C0000022);
  STATUS_INFO_LENGTH_MISMATCH  = NTStatus($C0000004);
  SEVERITY_ERROR               = NTStatus($C0000000);

type
  PSYSTEM_HANDLE_INFORMATION = ^SYSTEM_HANDLE_INFORMATION;
  SYSTEM_HANDLE_INFORMATION = packed record
    ProcessId       : dword;
    ObjectTypeNumber: byte;
    Flags           : byte;
    Handle          : word;
    pObject         : pointer;
    GrantedAccess   : dword;
  end;

  PSYSTEM_HANDLE_INFORMATION_EX = ^SYSTEM_HANDLE_INFORMATION_EX;
  SYSTEM_HANDLE_INFORMATION_EX = packed record
    NumberOfHandles: dword;
    Information    : array [0 .. 0] of SYSTEM_HANDLE_INFORMATION;
  end;

const
  ObjectNameInformation = 1;

type
  UNICODE_STRING = packed record
    Length       : word;
    MaximumLength: word;
    Buffer       : PWideChar;
  end;

type
  OBJECT_NAME_INFORMATION = record
    Name: UNICODE_STRING;

  end;

  PIO_STATUS_BLOCK = ^IO_STATUS_BLOCK;

  IO_STATUS_BLOCK = packed record
    Status     : NTStatus;
    Information: dword;
  end;

  PFILE_NAME_INFORMATION = ^FILE_NAME_INFORMATION;

  FILE_NAME_INFORMATION = packed record
    FileNameLength: ULONG;
    FileName      : array [0 .. MAX_PATH - 1] of WideChar;
  end;

type

  TDbgSettings = record
    dwSuspendType         : DWORD;
    dwDefaultExceptionMode: DWORD;
    bBreakOnNewDLL       : Boolean;
    bBreakOnNewPID       : Boolean;
	  bBreakOnNewTID       : Boolean;
	  bBreakOnExDLL        : Boolean;
	  bBreakOnExPID        : Boolean;
	  bBreakOnExTID        : Boolean;
	  bBreakOnModuleEP     : Boolean;
	  bBreakOnSystemEP     : Boolean;
    bBreakOnTLS          : Boolean;
	  bDebugChilds         : Boolean;
		bAutoLoadSymbols     : Boolean;
    bUseExceptionAssist  : Boolean;
    bNotWaitEventDbg     : Boolean;

		//dwBreakOnEPMode      : DWORD;
  end;

  TDLLStruct = record
	  sPath     : string;
		bLoaded   : Boolean;
		dwPID     : DWORD;
		dwBaseAdr : NativeUInt;
	end;
  PTDLLStruct = ^TDLLStruct;

  TThreadStruct = record
		dwTID     : DWORD;
		dwPID     : DWORD;
		dwExitCode: DWORD;
		dwEP      : UInt64;
		bSuspended: Boolean;
  end;
  PThreadStruct = ^TThreadStruct;

	 TPIDStruct = record
    dwPID           : DWORD;
		dwExitCode      : DWORD;
		dwBPRestoreFlag : DWORD;
    hProc           : THandle;
		sFileName       : string;
		dwEP            : UInt64;
    bKernelBP       : Boolean;
    bWOW64KernelBP  : Boolean;
		bRunning        : Boolean;
		bSymLoad        : Boolean;
		bTrapFlag       : Boolean;
    bTraceFlag      : Boolean;
    dwIBase         : UInt64;
	end;
  PPIDStruct = ^TPIDStruct;

  TBreakPoint = record
      bpxSize      : DWORD;
      bpxSlot      : DWORD;
      (*
	     |	DR_EXECUTE
	     |	DR_WRITE
	     |	DR_READ
	    *)
	    bpxType      : Byte;
      (*
	     |	0x0 - don�t keep
	     |  0x1 - keep
	     |  0x2 - step , remove it
	     |  0x3 - offset changed
	   	*)
      dwHandle     : DWORD;
      bpxAddress   : NativeUInt;
      bpxBaseOffset : NativeUInt;
	    bpxOldOffset  : NativeUInt;
      dwPID        : DWORD;
      OriginalByte : Byte;
      bRestoreBP   : Boolean;
      moduleName : PChar;
      bpxCallBack  : Pointer;
	end;
  BreakPoint = TBreakPoint;
  PBreakPoint = ^BreakPoint;

  TCustomException  = record
    dwExceptionType : DWORD;
		dwAction        : DWORD;
		dwHandler       : Pointer;
	end;
  PCustomException = ^TCustomException;

   // Win Api
   TOpenThread                = function(dwDesiredAccess:DWORD;bInheritHandle:BOOL;dwThreadId:DWORD):THANDLE; stdcall;
   TDebugBreakProcess         = function(processhandle:THandle):boolean; stdcall;
   TDebugActiveProcessStop    = function(pid: dword):boolean; stdcall;
   TDebugSetProcessKillOnExit = function(KillOnExit: boolean):boolean; stdcall;

   TOnDbgLogEvent     =  reference to procedure ( vTime : TTime;Msg: string ) ;
   TOnDbgString       =  reference to procedure ( sMessage: string;dwPID: DWORD);
   TOnThread          =  reference to procedure ( dwPID,dwTID: DWORD;dwEP : NativeUInt;bSuspended: Boolean;dwExitCode: DWORD;bFound: Boolean);
   TOnPID             =  reference to procedure ( dwPID: DWORD; sFile: string;dwExitCode: DWORD;dwEP,dwIBase: NativeUInt;bFound: Boolean);
   TOnDll             =  reference to procedure ( sDLLPath: string;dwPID: DWORD;dwEP: NativeUInt;bLoaded: Boolean);
   TOnDebuggerBreak   =  reference to procedure;
   TOnBPDeleted       =  reference to procedure ( bpOffset: NativeUInt);
   TOnNewBPAdded      =  reference to procedure (newBp : TBreakPoint; iType : Integer );
   TOnDebuggerEnd     =  reference to procedure;
   TOnNewPID          =  reference to procedure (sPid: string; nPid: Integer);

   TCallBackCustomBreakPoint   = function: DWORD; stdcall;
   TCallBackCustomExceptEvent  = function(debugevent: DEBUG_EVENT): DWORD; stdcall;

   function NtQueryInformationFile(FileHandle: THandle;
              IoStatusBlock: PIO_STATUS_BLOCK; FileInformation: pointer; Length: dword;
              FileInformationClass: dword): NTStatus; stdcall; external 'ntdll.dll';

implementation

end.
